package hello.Repository;


import hello.model.TransactionDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

//import java.awt.*;
import java.time.LocalDateTime;
import java.util.List;

public interface TransactionRepository extends CrudRepository<TransactionDetailsEntity, Long> {

    List<TransactionDetailsEntity> findByMerchantIdAndCreatedAtBetween(String merchantId, LocalDateTime startTime, LocalDateTime endTime);

    List<TransactionDetailsEntity> findByMerchantIdAndTransactionStatusAndCreatedAtBetween(String merchantId, String status, LocalDateTime startTime, LocalDateTime endTime);
}
