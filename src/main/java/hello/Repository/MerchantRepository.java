package hello.Repository;

import hello.model.MerchantsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MerchantRepository extends CrudRepository<MerchantsEntity, Long> {
//    Find by
MerchantsEntity findMerchantsEntityByEmailAndPassword(String email, String password);
}


