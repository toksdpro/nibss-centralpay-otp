
package hello;


import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

import hello.model.Bank;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.w3c.dom.Document;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import java.io.*;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import javax.xml.parsers.*;
import org.xml.sax.InputSource;
import org.w3c.dom.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String[] args)
	{
		SpringApplication.run(Application.class, args);
//		try {
//
//			System.err.println(bankList().toString());
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//		}
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {

			System.out.println("Let's inspect the beans provided by Spring Boot:");

			String[] beanNames = ctx.getBeanDefinitionNames();
			Arrays.sort(beanNames);
			for (String beanName : beanNames) {
				System.out.println(beanName);
			}

		};
	}



	public static List<Bank> bankList() throws MalformedURLException, IOException, NoSuchAlgorithmException, KeyManagementException {
		List<Bank> banks;
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		}
		};

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

//Code to make a webservice HTTP request
		String responseString = "";
		String outputString = "";
		String wsURL = "https://staging.nibss-plc.com.ng/CentralPayWebservice/CentralPayOperations";
		URL url = new URL(wsURL);
		URLConnection connection = url.openConnection();
		HttpURLConnection httpConn = (HttpURLConnection)connection;
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		String xmlInput = "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"    <Body>\n" +
				"        <listActiveBanks xmlns=\"http://web.nibss.com/\">\n" +
				"            <arg0 xmlns=\"\">00000001</arg0>\n" +
				"        </listActiveBanks>\n" +
				"    </Body>\n" +
				"</Envelope>";


		byte[] buffer = new byte[xmlInput.length()];
		buffer = xmlInput.getBytes();
		bout.write(buffer);
		byte[] b = bout.toByteArray();
		String SOAPAction = "https://staging.nibss-plc.com.ng/CentralPayWebservice/CentralPayOperations";
// Set the appropriate HTTP parameters.
		httpConn.setRequestProperty("Content-Length",
				String.valueOf(b.length));
		httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
		//httpConn.setRequestProperty("SOAPAction", SOAPAction);
		httpConn.setRequestMethod("POST");
		httpConn.setDoOutput(true);
		httpConn.setDoInput(true);
		OutputStream out = httpConn.getOutputStream();
//Write the content of the request to the outputstream of the HTTP Connection.
		out.write(b);
		out.close();
//Ready with sending the request.

//Read the response.
		InputStreamReader isr = null;
		if (httpConn.getResponseCode() == 200) {
			isr = new InputStreamReader(httpConn.getInputStream());
		} else {
			isr = new InputStreamReader(httpConn.getErrorStream());
		}

		BufferedReader in = new BufferedReader(isr);

//Write the SOAP message response to a String.
		while ((responseString = in.readLine()) != null) {
			outputString = outputString + responseString;
		}

//Parse the String output to a org.w3c.dom.Document and be able to reach every node with the org.w3c.dom API.
		Document document = parseXmlFile(outputString); // Write a separate method to parse the xml input.
		NodeList nodeLst = document.getElementsByTagName("return");
		String elementValue = nodeLst.item(0).getTextContent();
		//Node node = nodeLst.item(0).getFirstChild();
//		for (int i = 0; i < nodes.getLength(); i++) {
//			System.out.println(nodes.item(i).getNodeValue());
//		}
		//System.out.println(node.getNodeName());
		document = parseXmlFile(elementValue); // Write a separate method to parse the xml input.
		nodeLst = document.getElementsByTagName("bank");
		elementValue = nodeLst.item(0).getTextContent();
		banks = new ArrayList<Bank>();
		String bankcode, bankname;
		for (int i = 0; i < nodeLst.getLength(); i++) {
			//System.out.println(nodeLst.item(i).getNodeName());
			Element element = (Element) nodeLst.item(i);
//
			NodeList name = element.getElementsByTagName("bankCode");
			Element line = (Element) name.item(0);
			System.out.println("bankCode: " + getCharacterDataFromElement(line));
			bankcode = getCharacterDataFromElement(line);
			NodeList title = element.getElementsByTagName("bankName");
			line = (Element) title.item(0);
			System.out.println("bankName: " + getCharacterDataFromElement(line));
			bankname = getCharacterDataFromElement(line);
			banks.add(new Bank(Integer.parseInt(bankcode),bankname));
		}

/*//Write the SOAP message formatted to the console.
		String formattedSOAPResponse = formatXML(outputString); // Write a separate method to format the XML input.
		System.out.println(formattedSOAPResponse);*/
		return banks;
		//return outputString;

	}

	//format the XML in your String
	public static String formatXML(String unformattedXml) {
		try {
			Document document = parseXmlFile(unformattedXml);
			OutputFormat format = new OutputFormat(document);
			format.setIndenting(true);
			format.setIndent(3);
			format.setOmitXMLDeclaration(true);
			Writer out = new StringWriter();
			XMLSerializer serializer = new XMLSerializer(out, format);
			serializer.serialize(document);
			return out.toString();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static Document parseXmlFile(String in) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(in));
			return db.parse(is);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "?";
	}

}
