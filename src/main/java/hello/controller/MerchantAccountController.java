package hello.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import hello.controller.Request.LoginRequest;
import hello.controller.Response.LoginResponse;
import hello.controller.Response.StatisticsResponse;
import hello.controller.Response.TransactionReponse;
import hello.service.MerchantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.toIntExact;

@Controller
@RequestMapping("/merchantAccount")
public class MerchantAccountController {
    private final MerchantService merchantService;
    private final ObjectMapper objectMapper;

    public static final Logger logger = LoggerFactory.getLogger(MerchantService.class);

    @Autowired
    public MerchantAccountController(MerchantService merchantService, ObjectMapper objectMapper){
        this.merchantService = merchantService;
        this.objectMapper = objectMapper;

    }

    @CrossOrigin
    @RequestMapping(path = "authenticate", method = RequestMethod.POST)
    public ResponseEntity authenticateMerchant(@Valid @RequestBody LoginRequest loginRequest){

        try {
            LoginResponse loginResponse = merchantService.createLogin(loginRequest);
            return new ResponseEntity(loginResponse, HttpStatus.OK);
        }
        catch (Exception ex){
            ex.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(path = "statistics", method = RequestMethod.GET)
    public ResponseEntity<List<TransactionReponse>> getStatistics(
            @RequestParam(value="merchantId", required=true) String merchantId,
            @RequestParam(value="startTime", required=true) @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss") LocalDateTime startTime,
            @RequestParam(value="endTime", required=true) @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss") LocalDateTime endTime){

        try {
             StatisticsResponse statisticsResponse = merchantService.statistics(merchantId,startTime, endTime);
            return new ResponseEntity(statisticsResponse, HttpStatus.OK);
        }
        catch (Exception ex){
            ex.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(path = "transactions", method = RequestMethod.GET)
    public ResponseEntity<List<TransactionReponse>> getTransactions(
            @RequestParam(value="merchantId", required=true) String merchantId,
            @RequestParam(value="startTime", required=true) @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss") LocalDateTime startTime,
            @RequestParam(value="endTime", required=true) @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss") LocalDateTime endTime){

        try {
            List<TransactionReponse> transactionReponseList= merchantService.transactions(merchantId,startTime, endTime);
            return new ResponseEntity(transactionReponseList, HttpStatus.OK);
        }
        catch (Exception ex){
            ex.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


}