package hello.controller;


import hello.model.Bank;
import hello.service.NibbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/bank")
public class BankController {

    private final NibbService nibbService;

    @Value("${mes.merchant-id-fixed}")
    private String merchantId;

    @Autowired
    public BankController(NibbService nibbService) {
        this.nibbService = nibbService;
    }

    @RequestMapping(path= "bankList" ,method = RequestMethod.GET)
    public ResponseEntity bankList(@RequestParam String merchantID) throws Exception {
        System.out.print(merchantId);
        try{
            List<Bank> bankList = nibbService.bankList(merchantId);
            if(bankList == null){
                bankList = new ArrayList<>();
            }
            return new ResponseEntity(bankList, HttpStatus.OK);
        }
        catch (IllegalArgumentException ex){
            ex.printStackTrace();
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


}