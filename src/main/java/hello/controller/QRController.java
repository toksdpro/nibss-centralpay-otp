package hello.controller;


import hello.controller.Response.TransactionNotificationResponse;
import hello.service.InterswitchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/generateQrCode")
public class QRController {

    private final InterswitchService interswitchService;

    //@Value("${mes.merchant-id-fixed}")
    //private String merchantId;

    @Autowired
    public QRController(InterswitchService interswitchService) {
        this.interswitchService = interswitchService;
    }

    @RequestMapping(path= "GetImage" ,method = RequestMethod.GET)
    public ResponseEntity getImage(@RequestParam String amount, @RequestParam String transactionRef, @RequestParam String merchantName) {
    //public ResponseEntity getImage() {
        byte[] imgData = interswitchService.generateQrImage(amount, transactionRef, merchantName);
        //String imgData = interswitchService.generateQrImage(amount, transactionRef);
        if (imgData == null) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_PNG);
            return new ResponseEntity(imgData, headers, HttpStatus.OK);
        }
    }

    @RequestMapping(path= "GetTransactionStatus" ,method = RequestMethod.GET)
    public ResponseEntity getTransactionStatus(@RequestParam String transactionRef) {
        try {
            TransactionNotificationResponse transactionNotificationResponse = interswitchService.getTransacctionNotification(transactionRef);
            return new ResponseEntity(transactionNotificationResponse, HttpStatus.OK);
        }
        catch (Exception ex){
            ex.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

}