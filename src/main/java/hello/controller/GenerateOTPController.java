package hello.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import hello.controller.Request.CreateMandateRequest;
import hello.controller.Request.GenerateOTPRequest;
import hello.controller.Request.ValidateOTPRequest;
import hello.controller.Response.CreateMandateResponse;
import hello.controller.Response.GenerateOTPResponse;
import hello.controller.Response.ValidateOTPResponse;
import hello.service.NibbService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.socket.TextMessage;

//import javax.validation.Valid;

@Controller
@RequestMapping("/generateOTP")
public class GenerateOTPController {

    @Value("${mes.wsURL}")
    private String wsURL;

    @Value("${mes.merchant-id-fixed}")
    private String merchantIdFixed;

    @Value("${mes.merchant-id-variable}")
    private String merchantIdVariable;

    @Value("${mes.merchant-name-fixed}")
    private String merchantNameFixed;

    @Value("${mes.merchant-name-variable}")
    private String merchantNameVariable;

    @Value("${mes.secret-key-fixed}")
    private String secretKeyFixed;

    @Value("${mes.secret-key-variable}")
    private String secretKeyVariable;

    public static final Logger logger = LoggerFactory.getLogger(GenerateOTPController.class);
    private final NibbService nibbService;
    //    @Autowired
    public GenerateOTPController(NibbService nibbService){
        this.nibbService = nibbService;
    }
    //
    @RequestMapping(path = "generateOTP", method = RequestMethod.POST)
    public ResponseEntity createMandate(@RequestBody GenerateOTPRequest generateOTPRequest, final BindingResult errors) throws Exception{
        if(errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        try {
            if (Double.parseDouble(generateOTPRequest.getAmt()) > 1500000.00){

                generateOTPRequest.setBillerID(merchantIdFixed);
                generateOTPRequest.setBillerName(merchantNameFixed);
                GenerateOTPResponse generateOTPResponse = nibbService.generateOTPrequest(generateOTPRequest, wsURL, secretKeyFixed);
                return new ResponseEntity(generateOTPResponse, HttpStatus.OK);
            }

            else {

                generateOTPRequest.setBillerID(merchantIdVariable);
                generateOTPRequest.setBillerName(merchantNameVariable);
                GenerateOTPResponse generateOTPResponse = nibbService.generateOTPrequest(generateOTPRequest, wsURL, secretKeyVariable);
                return new ResponseEntity(generateOTPResponse, HttpStatus.OK);
            }

        }

        catch (Exception ex){
            ex.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


}