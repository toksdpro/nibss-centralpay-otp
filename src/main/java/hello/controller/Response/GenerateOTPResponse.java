package hello.controller.Response;

public class GenerateOTPResponse {

    private String mandateCode;
    private String bankCode;
    private String otp;
    private String billerID;
    private String billerName;
    private String amt;
    private String transType;
    private String billerTransId;
    private String referenceNumber;
    private String responseCode;
    private String hashValue;

    public GenerateOTPResponse() {
    }

    public GenerateOTPResponse(String mandateCode, String bankCode, String otp, String billerID, String billerName, String amt, String transType, String billerTransId, String referenceNumber, String responseCode, String hashValue) {
        this.mandateCode = mandateCode;
        this.bankCode = bankCode;
        this.otp = otp;
        this.billerID = billerID;
        this.billerName = billerName;
        this.amt = amt;
        this.transType = transType;
        this.billerTransId = billerTransId;
        this.referenceNumber = referenceNumber;
        this.responseCode = responseCode;
        this.hashValue = hashValue;
    }

    public String getMandateCode() {
        return mandateCode;
    }

    public void setMandateCode(String mandateCode) {
        this.mandateCode = mandateCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getBillerID() {
        return billerID;
    }

    public void setBillerID(String billerID) {
        this.billerID = billerID;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getBillerTransId() {
        return billerTransId;
    }

    public void setBillerTransId(String billerTransId) {
        this.billerTransId = billerTransId;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue;
    }
}
