package hello.controller.Response;

public class ValidateOTPResponse {

    private String mandateCode;
    private String bankCode;
    private String otp;
    private String billerID;
    private String billerName;
    private String amt;
    private String transType;
    private String billerTransId;
    private String ReferenceNumber;
    private String responseCode;
    private String paymentStatus;
    private String cPayRef;
    private String hashValue;

    public ValidateOTPResponse() {
    }

    public ValidateOTPResponse(String mandateCode, String bankCode, String otp, String billerID, String billerName, String amt, String transType, String billerTransId, String referenceNumber, String responseCode, String paymentStatus, String cPayRef, String hashValue) {
        this.mandateCode = mandateCode;
        this.bankCode = bankCode;
        this.otp = otp;
        this.billerID = billerID;
        this.billerName = billerName;
        this.amt = amt;
        this.transType = transType;
        this.billerTransId = billerTransId;
        ReferenceNumber = referenceNumber;
        this.responseCode = responseCode;
        this.paymentStatus = paymentStatus;
        this.cPayRef = cPayRef;
        this.hashValue = hashValue;
    }

    public String getMandateCode() {
        return mandateCode;
    }

    public void setMandateCode(String mandateCode) {
        this.mandateCode = mandateCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getBillerID() {
        return billerID;
    }

    public void setBillerID(String billerID) {
        this.billerID = billerID;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getBillerTransId() {
        return billerTransId;
    }

    public void setBillerTransId(String billerTransId) {
        this.billerTransId = billerTransId;
    }

    public String getReferenceNumber() {
        return ReferenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        ReferenceNumber = referenceNumber;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getcPayRef() {
        return cPayRef;
    }

    public void setcPayRef(String cPayRef) {
        this.cPayRef = cPayRef;
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue;
    }
}
