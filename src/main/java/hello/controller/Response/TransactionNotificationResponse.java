package hello.controller.Response;

public class TransactionNotificationResponse {

    private String responseCode;
    private String responseDescription;
    private String transactionReference;
    private String accountName;
    private String amount;


    public TransactionNotificationResponse() {
    }

    public TransactionNotificationResponse(String responseCode, String responseDescription, String transactionReference, String accountName, String amount) {
        this.responseCode = responseCode;
        this.responseDescription = responseDescription;
        this.transactionReference = transactionReference;
        this.accountName =  accountName;
        this.amount = amount;

    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) { this.transactionReference = transactionReference; }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAmount() { return amount; }

    public void setAmount(String amount) { this.amount = amount; }
}
