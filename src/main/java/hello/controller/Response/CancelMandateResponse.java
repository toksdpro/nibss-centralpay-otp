package hello.controller.Response;

public class CancelMandateResponse {

    private String mandateCode;
    private String bankCode;
    private String billerID;
    private String billerName;
    private String amt;
    private String transType;
    private String billerTransId;
    private String responseCode;
    private String hashValue;

    public CancelMandateResponse() {
    }

    public CancelMandateResponse(String mandateCode, String bankCode, String billerID, String billerName, String amt, String transType, String billerTransId, String responseCode, String hashValue) {
        this.mandateCode = mandateCode;
        this.bankCode = bankCode;
        this.billerID = billerID;
        this.billerName = billerName;
        this.amt = amt;
        this.transType = transType;
        this.billerTransId = billerTransId;
        this.responseCode = responseCode;
        this.hashValue = hashValue;
    }

    public String getMandateCode() {
        return mandateCode;
    }

    public void setMandateCode(String mandateCode) {
        this.mandateCode = mandateCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBillerID() {
        return billerID;
    }

    public void setBillerID(String billerID) {
        this.billerID = billerID;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getBillerTransId() {
        return billerTransId;
    }

    public void setBillerTransId(String billerTransId) {
        this.billerTransId = billerTransId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue;
    }
}
