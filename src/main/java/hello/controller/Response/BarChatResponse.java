package hello.controller.Response;

import lombok.Data;

import java.util.List;

@Data
public class BarChatResponse {

    private double captured;
    private double failed;
    private double success;

    private String date;
}
