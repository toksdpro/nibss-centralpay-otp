package hello.controller.Response;



import javax.validation.constraints.NotNull;


public class CreateMandateResponse {

    public String accountNo;
    public String accountName;
    public String transType;
    public String bankCode;
    public String billerID;
    public String billerName;
    public String billerTransId;
    private String mandateCode;
    private String responseCode;
    private String hashValue;

    public CreateMandateResponse() {
    }

    public CreateMandateResponse(String accountNo, String accountName, String transType, String bankCode, String billerID, String billerName, String billerTransId, String mandateCode, String responseCode, String hashValue) {
        this.accountNo = accountNo;
        this.accountName = accountName;
        this.transType = transType;
        this.bankCode = bankCode;
        this.billerID = billerID;
        this.billerName = billerName;
        this.billerTransId = billerTransId;
        this.mandateCode = mandateCode;
        this.responseCode = responseCode;
        this.hashValue = hashValue;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBillerID() {
        return billerID;
    }

    public void setBillerID(String billerID) {
        this.billerID = billerID;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getBillerTransId() {
        return billerTransId;
    }

    public void setBillerTransId(String billerTransId) {
        this.billerTransId = billerTransId;
    }

    public String getMandateCode() {
        return mandateCode;
    }

    public void setMandateCode(String mandateCode) {
        this.mandateCode = mandateCode;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue;
    }
}
