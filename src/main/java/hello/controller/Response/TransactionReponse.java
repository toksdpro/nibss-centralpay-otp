package hello.controller.Response;

import lombok.Data;

@Data
public class TransactionReponse {

    private String transId;
    private String narration;
    private double amount;
    private String merchantId;
    private String status;
    private String date;
}
