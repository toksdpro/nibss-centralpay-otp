package hello.controller.Response;

import lombok.Data;

@Data
public class LoginResponse {
    private String token;
    private  String email;
    private  String businessName;
    private String merchantId;
}
