package hello.controller.Response;

import lombok.Data;

import java.util.List;

@Data
public class StatisticsResponse {

    private double revenue;
    private double settlement;
    private double success;
    private double failed;
    private double captured;

    private List<BarChatResponse> barChart;

}
