package hello.controller;

import hello.controller.Request.CancelMandateRequest;
import hello.controller.Request.CreateMandateRequest;
import hello.controller.Response.CancelMandateResponse;
import hello.controller.Response.CreateMandateResponse;
import hello.service.NibbService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.socket.TextMessage;

//import javax.validation.Valid;

@Controller
@RequestMapping("/cancelMandate")
public class CancelMandateController {

    @Value("${mes.wsURL}")
    private String wsURL;

    @Value("${mes.merchant-id-fixed}")
    private String merchantIdFixed;

    @Value("${mes.merchant-id-variable}")
    private String merchantIdVariable;

    @Value("${mes.merchant-name-fixed}")
    private String merchantNameFixed;

    @Value("${mes.merchant-name-variable}")
    private String merchantNameVariable;

    @Value("${mes.secret-key-fixed}")
    private String secretKeyFixed;

    @Value("${mes.secret-key-variable}")
    private String secretKeyVariable;

    public static final Logger logger = LoggerFactory.getLogger(CancelMandateController.class);
    private final NibbService nibbService;
//    @Autowired
    public CancelMandateController(NibbService nibbService){
        this.nibbService = nibbService;
    }


    @RequestMapping(path = "cancelMandate", method = RequestMethod.POST)
    public ResponseEntity cancelMandate(@RequestBody CancelMandateRequest cancelMandateRequest, final BindingResult errors) throws Exception{
        if(errors.hasErrors()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        try {

            cancelMandateRequest.setBillerID(merchantIdFixed);
            cancelMandateRequest.setBillerName(merchantNameFixed);
            CancelMandateResponse cancelMandateResponse = nibbService.cancelMandateRequest(cancelMandateRequest, wsURL, secretKeyFixed);
            //return new ResponseEntity(merchantAccount, HttpStatus.OK);
            //System.out.println(createMandateRequest.accountName);
            /*CreateMandateResponse createMandateResponse = new CreateMandateResponse(createMandateRequest.getAccountNo(),
                    createMandateRequest.accountName, createMandateRequest.transType, createMandateRequest.getBillerID(),
                    createMandateRequest.getBillerID(), createMandateRequest.getBillerID(), createMandateRequest.getBillerTransId(),
                    "", "", "");*/
            return new ResponseEntity(cancelMandateResponse, HttpStatus.OK);
        }

        catch (Exception ex){
            ex.printStackTrace();
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


}