package hello.controller.Request;

public class GenerateOTPRequest {

    private String mandateCode;
    private String bankCode;
    private String billerID;
    private String billerName;
    private String amt;
    private String transType;
    private String billerTransId;

    public String getMandateCode() {
        return mandateCode;
    }

    public void setMandateCode(String mandateCode) {
        this.mandateCode = mandateCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBillerID() {
        return billerID;
    }

    public void setBillerID(String billerID) {
        this.billerID = billerID;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getBillerTransId() {
        return billerTransId;
    }

    public void setBillerTransId(String billerTransId) {
        this.billerTransId = billerTransId;
    }
}
