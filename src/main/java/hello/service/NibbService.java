package hello.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import hello.controller.Request.CancelMandateRequest;
import hello.controller.Request.CreateMandateRequest;
import hello.controller.Request.GenerateOTPRequest;
import hello.controller.Request.ValidateOTPRequest;
import hello.controller.Response.CancelMandateResponse;
import hello.controller.Response.CreateMandateResponse;
import hello.controller.Response.GenerateOTPResponse;
import hello.controller.Response.ValidateOTPResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import hello.model.Bank;
import org.w3c.dom.Document;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import java.io.*;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import javax.xml.parsers.*;
import org.xml.sax.InputSource;
import org.w3c.dom.*;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;


@Service
public class NibbService {

    private static final AtomicLong counter = new AtomicLong();

    public static int PRETTY_PRINT_INDENT_FACTOR = 4;
    private static List<Bank> banks;

    @Value("${mes.wsURL}")
    private String wsURL;

    public List<Bank> bankList(String merchantID) throws MalformedURLException, IOException, NoSuchAlgorithmException, KeyManagementException {
        List<Bank> banks;
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

//Code to make a webservice HTTP request
        String responseString = "";
        String outputString = "";

        URL url = new URL(wsURL);
        URLConnection connection = url.openConnection();
        HttpURLConnection httpConn = (HttpURLConnection)connection;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        String xmlInput = "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "    <Body>\n" +
                "        <listActiveBanksOTP xmlns=\"http://web.nibss.com/\">\n" +
                "            <arg0 xmlns=\"\">"+merchantID+"</arg0>\n" +
                "        </listActiveBanksOTP>\n" +
                "    </Body>\n" +
                "</Envelope>";


        byte[] buffer = new byte[xmlInput.length()];
        buffer = xmlInput.getBytes();
        bout.write(buffer);
        byte[] b = bout.toByteArray();

// Set the appropriate HTTP parameters.
        httpConn.setRequestProperty("Content-Length",
                String.valueOf(b.length));
        httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        //httpConn.setRequestProperty("SOAPAction", SOAPAction);
        httpConn.setRequestMethod("POST");
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        OutputStream out = httpConn.getOutputStream();
//Write the content of the request to the outputstream of the HTTP Connection.
        out.write(b);
        out.close();
//Ready with sending the request.

//Read the response.
        InputStreamReader isr = null;
        if (httpConn.getResponseCode() == 200) {
            isr = new InputStreamReader(httpConn.getInputStream());
        } else {
            isr = new InputStreamReader(httpConn.getErrorStream());
        }

        BufferedReader in = new BufferedReader(isr);

//Write the SOAP message response to a String.
        while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;
        }
        System.out.print(outputString);
//Parse the String output to a org.w3c.dom.Document and be able to reach every node with the org.w3c.dom API.
        Document document = parseXmlFile(outputString); // Write a separate method to parse the xml input.
        NodeList nodeLst = document.getElementsByTagName("return");
        String elementValue = nodeLst.item(0).getTextContent();
        //Node node = nodeLst.item(0).getFirstChild();
//		for (int i = 0; i < nodes.getLength(); i++) {
//			System.out.println(nodes.item(i).getNodeValue());
//		}
        //System.out.println(node.getNodeName());
        document = parseXmlFile(elementValue); // Write a separate method to parse the xml input.
        nodeLst = document.getElementsByTagName("bank");
        elementValue = nodeLst.item(0).getTextContent();
        banks = new ArrayList<Bank>();
        String bankcode, bankname;
        for (int i = 0; i < nodeLst.getLength(); i++) {
            //System.out.println(nodeLst.item(i).getNodeName());
            Element element = (Element) nodeLst.item(i);
//
            NodeList name = element.getElementsByTagName("bankCode");
            Element line = (Element) name.item(0);
            System.out.println("bankCode: " + getCharacterDataFromElement(line));
            bankcode = getCharacterDataFromElement(line);
            NodeList title = element.getElementsByTagName("bankName");
            line = (Element) title.item(0);
            System.out.println("bankName: " + getCharacterDataFromElement(line));
            bankname = getCharacterDataFromElement(line);
            banks.add(new Bank(Integer.parseInt(bankcode),bankname));
        }

/*//Write the SOAP message formatted to the console.
		String formattedSOAPResponse = formatXML(outputString); // Write a separate method to format the XML input.
		System.out.println(formattedSOAPResponse);*/
        return banks;
        //return outputString;

    }

    //format the XML in your String
    public static String formatXML(String unformattedXml) {
        try {
            Document document = parseXmlFile(unformattedXml);
            OutputFormat format = new OutputFormat(document);
            format.setIndenting(true);
            format.setIndent(3);
            format.setOmitXMLDeclaration(true);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);
            return out.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Document parseXmlFile(String in) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(in));
            return db.parse(is);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "?";
    }

    private String getHashValue(String stringText, String key)
    {
        //System.out.println(this.SECRET_KEY_FIXED);
        System.out.println(key);
        String sha256hex = DigestUtils.sha256Hex(stringText + key);
        //String sha256hex = get_SHA_1_XML(SECRET_KEY, stringText);
        return sha256hex;
    }

   private String XmltoJson(String xml) {
        try {
            JSONObject xmlJSONObj = XML.toJSONObject(xml);
            String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);

            return jsonPrettyPrintString;
        } catch (JSONException je) {
            System.out.println(je.toString());
        }
        return null;
    }

    public CreateMandateResponse createMandateRequest(CreateMandateRequest createMandateRequest, String wsURL, String key) throws NoSuchAlgorithmException, KeyManagementException, IOException {

        String endXML = "</CreateMandateRequest>";

        String startXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><CreateMandateRequest><AcctNumber>" + createMandateRequest.getAccountNo() + "</AcctNumber>" + "<AcctName>" + createMandateRequest.getAccountName() + "</AcctName>" + "<TransType>" + createMandateRequest.getTransType() + "</TransType>" + "<BankCode>" + appendZerotoBankCode(createMandateRequest.getBankCode()) + "</BankCode>" + "<BillerID>" + createMandateRequest.getBillerID()+ "</BillerID>" + "<BillerName>" + createMandateRequest.getBillerName() + "</BillerName>" + "<BillerTransId>" + createMandateRequest.getBillerTransId() + "</BillerTransId>";

        String hashXML = startXML + endXML;

        String hashValue = getHashValue(hashXML, key);
        startXML = startXML + "<HashValue>" + hashValue + "</HashValue>" + endXML;

        System.out.println(startXML);

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

//Code to make a webservice HTTP request
        String responseString = "";
        String outputString = "";

        URL url = new URL(wsURL);
        URLConnection connection = url.openConnection();
        HttpURLConnection httpConn = (HttpURLConnection)connection;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        String xmlInput = "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "    <Body>\n" +
                "        <createMandateRequest xmlns=\"http://web.nibss.com/\">\n" +
                "            <arg0 xmlns=\"\"><![CDATA["+startXML+"]]></arg0>\n" +
                "        </createMandateRequest>\n" +
                "    </Body>\n" +
                "</Envelope>";


        byte[] buffer = new byte[xmlInput.length()];
        buffer = xmlInput.getBytes();
        bout.write(buffer);
        byte[] b = bout.toByteArray();

// Set the appropriate HTTP parameters.
        httpConn.setRequestProperty("Content-Length",
                String.valueOf(b.length));
        httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        //httpConn.setRequestProperty("SOAPAction", SOAPAction);
        httpConn.setRequestMethod("POST");
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        OutputStream out = httpConn.getOutputStream();
//Write the content of the request to the outputstream of the HTTP Connection.
        out.write(b);
        out.close();
//Ready with sending the request.

//Read the response.
        System.out.println(httpConn.getResponseCode());
        System.out.println(httpConn.getResponseMessage());
        InputStreamReader isr = null;
        if (httpConn.getResponseCode() == 200) {

            isr = new InputStreamReader(httpConn.getInputStream());
        } else {
            isr = new InputStreamReader(httpConn.getErrorStream());
        }

        BufferedReader in = new BufferedReader(isr);

//Write the SOAP message response to a String.
        while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;
        }

        System.out.println(outputString);
        //return XmltoJson(outputString);
        /*String outputString = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ns2:createMandateRequestResponse xmlns:ns2=\"http://web.nibss.com/\">" +
                "<return>&lt;?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?&gt;&lt;CreateMandateResponse&gt;&lt;AcctNumber&gt;0719774102&lt;/AcctNumber&gt;&lt;AcctName&gt;Aniekan Samuel&lt;/AcctName&gt;&lt;TransType&gt;1&lt;/" +
                "TransType&gt;&lt;BankCode&gt;044&lt;/BankCode&gt;&lt;BillerID&gt;NIBSS0000000123&lt;/BillerID&gt;&lt;BillerName&gt;NetPlusDotCom&lt;/BillerName&gt;&lt;BillerTransId&gt;10456211453188" +
                "345&lt;/BillerTransId&gt;&lt;MandateCode&gt;04400000000001027806&lt;/MandateCode&gt;&lt;ResponseCode&gt;00&lt;/ResponseCode&gt;&lt;HashValue&gt;EBAD116A098724CC0F226AA1F46FA2F99FA41F" +
                "BD3F15720D72141241ACEA97BD&lt;/HashValue&gt;&lt;/CreateMandateResponse&gt;</return></ns2:createMandateRequestResponse></soap:Body></soap:Envelope>";*/
        //Parse the String output to a org.w3c.dom.Document and be able to reach every node with the org.w3c.dom API.
        Document document = parseXmlFile(outputString);
        NodeList nodeLst = document.getElementsByTagName("return");
        String elementValue = nodeLst.item(0).getTextContent();

        document = parseXmlFile(elementValue);
        nodeLst = document.getElementsByTagName("CreateMandateResponse");
        elementValue = nodeLst.item(0).getTextContent();
        CreateMandateResponse createMandateResponse = new CreateMandateResponse();
        for (int i = 0; i < nodeLst.getLength(); i++) {
            //System.out.println(nodeLst.item(i).getNodeName());
            Element element = (Element) nodeLst.item(i);
//
            NodeList accountNo = element.getElementsByTagName("AcctNumber");
            Element line = (Element) accountNo.item(0);
            System.out.println("AcctNumber: " + getCharacterDataFromElement(line));
            createMandateResponse.setAccountNo(getCharacterDataFromElement(line));

            NodeList acctName = element.getElementsByTagName("AcctName");
            line = (Element) acctName.item(0);
            System.out.println("AcctName: " + getCharacterDataFromElement(line));
            createMandateResponse.setAccountName(getCharacterDataFromElement(line));

            NodeList transType = element.getElementsByTagName("TransType");
            line = (Element) transType.item(0);
            System.out.println("TransType: " + getCharacterDataFromElement(line));
            createMandateResponse.setTransType(getCharacterDataFromElement(line));

            NodeList bankCode = element.getElementsByTagName("BankCode");
            line = (Element) bankCode.item(0);
            System.out.println("BankCode: " + getCharacterDataFromElement(line));
            createMandateResponse.setBankCode(getCharacterDataFromElement(line));

            NodeList billerID = element.getElementsByTagName("BillerID");
            line = (Element) billerID.item(0);
            System.out.println("BillerID: " + getCharacterDataFromElement(line));
            createMandateResponse.setBillerID(getCharacterDataFromElement(line));

            NodeList billerName = element.getElementsByTagName("BillerName");
            line = (Element) billerName.item(0);
            System.out.println("BillerName: " + getCharacterDataFromElement(line));
            createMandateResponse.setBillerName(getCharacterDataFromElement(line));

            NodeList billerTransId = element.getElementsByTagName("BillerTransId");
            line = (Element) billerTransId.item(0);
            System.out.println("BillerTransId: " + getCharacterDataFromElement(line));
            createMandateResponse.setBillerTransId(getCharacterDataFromElement(line));

            NodeList mandateCode = element.getElementsByTagName("MandateCode");
            line = (Element) mandateCode.item(0);
            System.out.println("MandateCode: " + getCharacterDataFromElement(line));
            createMandateResponse.setMandateCode(getCharacterDataFromElement(line));

            NodeList responseCode = element.getElementsByTagName("ResponseCode");
            line = (Element) responseCode.item(0);
            System.out.println("ResponseCode: " + getCharacterDataFromElement(line));
            createMandateResponse.setResponseCode(getCharacterDataFromElement(line));

        }

        return createMandateResponse;

    }


    public ValidateOTPResponse validateOTPrequest(ValidateOTPRequest validateOTPRequest, String wsURL, String key) throws NoSuchAlgorithmException, KeyManagementException, IOException {

        String endXML = "</ValidateOTPRequest>";

        String startXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><ValidateOTPRequest><AcctNumber>" + validateOTPRequest.getAccountNo() + "</AcctNumber>" + "<AcctName>" + validateOTPRequest.getAccountName() + "</AcctName>"  + "<MandateCode>" + validateOTPRequest.getMandateCode() + "</MandateCode>" + "<TransType>" + validateOTPRequest.getTransType() + "</TransType>" + "<OTP>" + validateOTPRequest.getOtp().trim() + "</OTP>" + "<BankCode>" + appendZerotoBankCode(validateOTPRequest.getBankCode()) + "</BankCode>" + "<BillerID>" + validateOTPRequest.getBillerID() + "</BillerID>" + "<BillerName>" + validateOTPRequest.getBillerName() + "</BillerName>" + "<Amount>" + validateOTPRequest.getAmt() + "</Amount>" + "<BillerTransId>" + validateOTPRequest.getBillerTransId() + "</BillerTransId>";

        String hashXML = startXML + endXML;

        String hashValue = getHashValue(hashXML, key);
        startXML = startXML + "<HashValue>" + hashValue + "</HashValue>" + endXML;

        System.out.println(startXML);

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

//Code to make a webservice HTTP request
        String responseString = "";
        String outputString = "";

        URL url = new URL(wsURL);
        URLConnection connection = url.openConnection();
        HttpURLConnection httpConn = (HttpURLConnection)connection;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        String xmlInput = "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "    <Body>\n" +
                "        <validateOTPRequest xmlns=\"http://web.nibss.com/\">\n" +
                "            <arg0 xmlns=\"\"><![CDATA["+startXML+"]]></arg0>\n" +
                "        </validateOTPRequest>\n" +
                "    </Body>\n" +
                "</Envelope>";


        byte[] buffer = new byte[xmlInput.length()];
        buffer = xmlInput.getBytes();
        bout.write(buffer);
        byte[] b = bout.toByteArray();

// Set the appropriate HTTP parameters.
        httpConn.setRequestProperty("Content-Length",
                String.valueOf(b.length));
        httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        //httpConn.setRequestProperty("SOAPAction", SOAPAction);
        httpConn.setRequestMethod("POST");
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        OutputStream out = httpConn.getOutputStream();
//Write the content of the request to the outputstream of the HTTP Connection.
        out.write(b);
        out.close();
//Ready with sending the request.

//Read the response.
        System.out.println(httpConn.getResponseCode());
        System.out.println(httpConn.getResponseMessage());
        InputStreamReader isr = null;
        if (httpConn.getResponseCode() == 200) {

            isr = new InputStreamReader(httpConn.getInputStream());
        } else {
            isr = new InputStreamReader(httpConn.getErrorStream());
        }

        BufferedReader in = new BufferedReader(isr);

//Write the SOAP message response to a String.
        while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;
        }

        System.out.println(outputString);
        Document document = parseXmlFile(outputString);
        NodeList nodeLst = document.getElementsByTagName("return");
        String elementValue = nodeLst.item(0).getTextContent();

        document = parseXmlFile(elementValue);
        nodeLst = document.getElementsByTagName("ValidateOTPResponse");
        elementValue = nodeLst.item(0).getTextContent();
        ValidateOTPResponse validateOTPResponse = new ValidateOTPResponse();
        for (int i = 0; i < nodeLst.getLength(); i++) {
            //System.out.println(nodeLst.item(i).getNodeName());
            Element element = (Element) nodeLst.item(i);
//
            NodeList mandateCode = element.getElementsByTagName("MandateCode");
            Element line = (Element) mandateCode.item(0);
            System.out.println("MandateCode: " + getCharacterDataFromElement(line));
            validateOTPResponse.setMandateCode(getCharacterDataFromElement(line));

            NodeList transType = element.getElementsByTagName("TransType");
            line = (Element) transType.item(0);
            System.out.println("TransType: " + getCharacterDataFromElement(line));
            validateOTPResponse.setTransType(getCharacterDataFromElement(line));

            NodeList bankCode = element.getElementsByTagName("BankCode");
            line = (Element) bankCode.item(0);
            System.out.println("BankCode: " + getCharacterDataFromElement(line));
            validateOTPResponse.setBankCode(getCharacterDataFromElement(line));

            NodeList billerID = element.getElementsByTagName("BillerID");
            line = (Element) billerID.item(0);
            System.out.println("BillerID: " + getCharacterDataFromElement(line));
            validateOTPResponse.setBillerID(getCharacterDataFromElement(line));

            NodeList billerName = element.getElementsByTagName("BillerName");
            line = (Element) billerName.item(0);
            System.out.println("BillerName: " + getCharacterDataFromElement(line));
            validateOTPResponse.setBillerName(getCharacterDataFromElement(line));

            NodeList amount  = element.getElementsByTagName("Amount");
            line = (Element) amount.item(0);
            String amt = getCharacterDataFromElement(line);
            System.out.println("Amount : " + amt.substring(0,amt.length()-2));
            //System.out.println("Amount : " + getCharacterDataFromElement(line));
            validateOTPResponse.setAmt(amt.substring(0,amt.length()-2));

            NodeList billerTransId = element.getElementsByTagName("BillerTransId");
            line = (Element) billerTransId.item(0);
            System.out.println("BillerTransId: " + getCharacterDataFromElement(line));
            validateOTPResponse.setBillerTransId(getCharacterDataFromElement(line));

            NodeList rNumber  = element.getElementsByTagName("ReferenceNumber");
            line = (Element) rNumber.item(0);
            System.out.println("ReferenceNumber : " + getCharacterDataFromElement(line));
            validateOTPResponse.setReferenceNumber(getCharacterDataFromElement(line));

            NodeList responseCode = element.getElementsByTagName("ResponseCode");
            line = (Element) responseCode.item(0);
            System.out.println("ResponseCode: " + getCharacterDataFromElement(line));
            validateOTPResponse.setResponseCode(getCharacterDataFromElement(line));

            NodeList paymentStatus = element.getElementsByTagName("PaymentStatus");
            line = (Element) paymentStatus.item(0);
            System.out.println("PaymentStatus: " + getCharacterDataFromElement(line));
            validateOTPResponse.setPaymentStatus(getCharacterDataFromElement(line));

            NodeList cPayRef = element.getElementsByTagName("CPayRef");
            line = (Element) cPayRef.item(0);
            System.out.println("CPayRef: " + getCharacterDataFromElement(line));
            validateOTPResponse.setcPayRef(getCharacterDataFromElement(line));

        }

        return validateOTPResponse;
    }


    public GenerateOTPResponse generateOTPrequest(GenerateOTPRequest generateOTPRequest, String wsURL, String key) throws NoSuchAlgorithmException, KeyManagementException, IOException {

        String endXML = "</GenerateOTPRequest>";

        String startXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><GenerateOTPRequest><MandateCode>" + generateOTPRequest.getMandateCode() + "</MandateCode>" + "<TransType>" + generateOTPRequest.getTransType() + "</TransType>" + "<BankCode>" + appendZerotoBankCode(generateOTPRequest.getBankCode()) + "</BankCode>" + "<BillerID>" + generateOTPRequest.getBillerID() + "</BillerID>" + "<BillerName>" + generateOTPRequest.getBillerName() + "</BillerName>" + "<Amount>" + generateOTPRequest.getAmt() + "</Amount>"  + "<BillerTransId>" + generateOTPRequest.getBillerTransId() + "</BillerTransId>";

        String hashXML = startXML + endXML;

        String hashValue = getHashValue(hashXML, key);
        startXML = startXML + "<HashValue>" + hashValue + "</HashValue>" + endXML;

        System.out.println(startXML);

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

//Code to make a webservice HTTP request
        String responseString = "";
        String outputString = "";

        URL url = new URL(wsURL);
        URLConnection connection = url.openConnection();
        HttpURLConnection httpConn = (HttpURLConnection)connection;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        String xmlInput = "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "    <Body>\n" +
                "        <generateOTPRequest xmlns=\"http://web.nibss.com/\">\n" +
                "            <arg0 xmlns=\"\"><![CDATA["+startXML+"]]></arg0>\n" +
                "        </generateOTPRequest>\n" +
                "    </Body>\n" +
                "</Envelope>";


        byte[] buffer = new byte[xmlInput.length()];
        buffer = xmlInput.getBytes();
        bout.write(buffer);
        byte[] b = bout.toByteArray();

// Set the appropriate HTTP parameters.
        httpConn.setRequestProperty("Content-Length",
                String.valueOf(b.length));
        httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        //httpConn.setRequestProperty("SOAPAction", SOAPAction);
        httpConn.setRequestMethod("POST");
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        OutputStream out = httpConn.getOutputStream();
//Write the content of the request to the outputstream of the HTTP Connection.
        out.write(b);
        out.close();
//Ready with sending the request.

//Read the response.
        System.out.println(httpConn.getResponseCode());
        System.out.println(httpConn.getResponseMessage());
        InputStreamReader isr = null;
        if (httpConn.getResponseCode() == 200) {

            isr = new InputStreamReader(httpConn.getInputStream());
        } else {
            isr = new InputStreamReader(httpConn.getErrorStream());
        }

        BufferedReader in = new BufferedReader(isr);

//Write the SOAP message response to a String.
        while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;
        }

        System.out.println(outputString);
        Document document = parseXmlFile(outputString);
        NodeList nodeLst = document.getElementsByTagName("return");
        String elementValue = nodeLst.item(0).getTextContent();

        document = parseXmlFile(elementValue);
        nodeLst = document.getElementsByTagName("GenerateOTPResponse");
        elementValue = nodeLst.item(0).getTextContent();
        GenerateOTPResponse generateOTPResponse = new GenerateOTPResponse();
        for (int i = 0; i < nodeLst.getLength(); i++) {
            //System.out.println(nodeLst.item(i).getNodeName());
            Element element = (Element) nodeLst.item(i);
//
            NodeList mandateCode = element.getElementsByTagName("MandateCode");
            Element line = (Element) mandateCode.item(0);
            System.out.println("MandateCode: " + getCharacterDataFromElement(line));
            generateOTPResponse.setMandateCode(getCharacterDataFromElement(line));

            NodeList transType = element.getElementsByTagName("TransType");
            line = (Element) transType.item(0);
            System.out.println("TransType: " + getCharacterDataFromElement(line));
            generateOTPResponse.setTransType(getCharacterDataFromElement(line));

            NodeList bankCode = element.getElementsByTagName("BankCode");
            line = (Element) bankCode.item(0);
            System.out.println("BankCode: " + getCharacterDataFromElement(line));
            generateOTPResponse.setBankCode(getCharacterDataFromElement(line));

            NodeList billerID = element.getElementsByTagName("BillerID");
            line = (Element) billerID.item(0);
            System.out.println("BillerID: " + getCharacterDataFromElement(line));
            generateOTPResponse.setBillerID(getCharacterDataFromElement(line));

            NodeList billerName = element.getElementsByTagName("BillerName");
            line = (Element) billerName.item(0);
            System.out.println("BillerName: " + getCharacterDataFromElement(line));
            generateOTPResponse.setBillerName(getCharacterDataFromElement(line));

            NodeList amount  = element.getElementsByTagName("Amount");
            line = (Element) amount.item(0);
            String amt = getCharacterDataFromElement(line);
            System.out.println("Amount : " + amt.substring(0,amt.length()-2));
            generateOTPResponse.setAmt(amt.substring(0,amt.length()-2));

            NodeList billerTransId = element.getElementsByTagName("BillerTransId");
            line = (Element) billerTransId.item(0);
            System.out.println("BillerTransId: " + getCharacterDataFromElement(line));
            generateOTPResponse.setBillerTransId(getCharacterDataFromElement(line));

            NodeList referenceNumber  = element.getElementsByTagName("ReferenceNumber");
            line = (Element) referenceNumber.item(0);
            System.out.println("ReferenceNumber : " + getCharacterDataFromElement(line));
            generateOTPResponse.setReferenceNumber(getCharacterDataFromElement(line));

            NodeList responseCode = element.getElementsByTagName("ResponseCode");
            line = (Element) responseCode.item(0);
            System.out.println("ResponseCode: " + getCharacterDataFromElement(line));
            generateOTPResponse.setResponseCode(getCharacterDataFromElement(line));

        }

        return generateOTPResponse;

    }

    public CancelMandateResponse cancelMandateRequest(CancelMandateRequest cancelMandateRequest, String wsURL, String key) throws NoSuchAlgorithmException, KeyManagementException, IOException {

        String endXML = "</CancelMandateRequest>";

        String startXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><CancelMandateRequest><MandateCode>" + cancelMandateRequest.getMandateCode() + "</MandateCode>" + "<TransType>" + cancelMandateRequest.getTransType() + "</TransType>" + "<BankCode>" + appendZerotoBankCode(cancelMandateRequest.getBankCode()) + "</BankCode>" + "<BillerID>" + cancelMandateRequest.getBillerID() + "</BillerID>" + "<BillerName>" + cancelMandateRequest.getBillerName()+ "</BillerName>" + "<BillerTransId>" + cancelMandateRequest.getBillerTransId() + "</BillerTransId>";

        String hashXML = startXML + endXML;

        String hashValue = getHashValue(hashXML, key);
        startXML = startXML + "<HashValue>" + hashValue + "</HashValue>" + endXML;

        System.out.println(startXML);

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

//Code to make a webservice HTTP request
        String responseString = "";
        String outputString = "";

        URL url = new URL(wsURL);
        URLConnection connection = url.openConnection();
        HttpURLConnection httpConn = (HttpURLConnection)connection;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        String xmlInput = "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "    <Body>\n" +
                "        <cancelMandate xmlns=\"http://web.nibss.com/\">\n" +
                "            <arg0 xmlns=\"\"><![CDATA["+startXML+"]]></arg0>\n" +
                "        </cancelMandate>\n" +
                "    </Body>\n" +
                "</Envelope>";


        byte[] buffer = new byte[xmlInput.length()];
        buffer = xmlInput.getBytes();
        bout.write(buffer);
        byte[] b = bout.toByteArray();

// Set the appropriate HTTP parameters.
        httpConn.setRequestProperty("Content-Length",
                String.valueOf(b.length));
        httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        //httpConn.setRequestProperty("SOAPAction", SOAPAction);
        httpConn.setRequestMethod("POST");
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        OutputStream out = httpConn.getOutputStream();
//Write the content of the request to the outputstream of the HTTP Connection.
        out.write(b);
        out.close();
//Ready with sending the request.

//Read the response.
        System.out.println(httpConn.getResponseCode());
        System.out.println(httpConn.getResponseMessage());
        InputStreamReader isr = null;
        if (httpConn.getResponseCode() == 200) {

            isr = new InputStreamReader(httpConn.getInputStream());
        } else {
            isr = new InputStreamReader(httpConn.getErrorStream());
        }

        BufferedReader in = new BufferedReader(isr);

//Write the SOAP message response to a String.
        while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;
        }

        System.out.println(outputString);
        //return XmltoJson(outputString);
        /*String outputString = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ns2:createMandateRequestResponse xmlns:ns2=\"http://web.nibss.com/\">" +
                "<return>&lt;?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?&gt;&lt;CreateMandateResponse&gt;&lt;AcctNumber&gt;0719774102&lt;/AcctNumber&gt;&lt;AcctName&gt;Aniekan Samuel&lt;/AcctName&gt;&lt;TransType&gt;1&lt;/" +
                "TransType&gt;&lt;BankCode&gt;044&lt;/BankCode&gt;&lt;BillerID&gt;NIBSS0000000123&lt;/BillerID&gt;&lt;BillerName&gt;NetPlusDotCom&lt;/BillerName&gt;&lt;BillerTransId&gt;10456211453188" +
                "345&lt;/BillerTransId&gt;&lt;MandateCode&gt;04400000000001027806&lt;/MandateCode&gt;&lt;ResponseCode&gt;00&lt;/ResponseCode&gt;&lt;HashValue&gt;EBAD116A098724CC0F226AA1F46FA2F99FA41F" +
                "BD3F15720D72141241ACEA97BD&lt;/HashValue&gt;&lt;/CreateMandateResponse&gt;</return></ns2:createMandateRequestResponse></soap:Body></soap:Envelope>";*/
        //Parse the String output to a org.w3c.dom.Document and be able to reach every node with the org.w3c.dom API.
        Document document = parseXmlFile(outputString);
        NodeList nodeLst = document.getElementsByTagName("return");
        String elementValue = nodeLst.item(0).getTextContent();

        document = parseXmlFile(elementValue);
        nodeLst = document.getElementsByTagName("CancelMandateResponse");
        elementValue = nodeLst.item(0).getTextContent();
        CancelMandateResponse cancelMandateResponse = new CancelMandateResponse();
        for (int i = 0; i < nodeLst.getLength(); i++) {
            //System.out.println(nodeLst.item(i).getNodeName());
            Element element = (Element) nodeLst.item(i);
//
            NodeList mandateCode = element.getElementsByTagName("MandateCode");
            Element line = (Element) mandateCode.item(0);
            System.out.println("MandateCode: " + getCharacterDataFromElement(line));
            cancelMandateResponse.setMandateCode(getCharacterDataFromElement(line));

            NodeList transType = element.getElementsByTagName("TransType");
            line = (Element) transType.item(0);
            System.out.println("TransType: " + getCharacterDataFromElement(line));
            cancelMandateResponse.setTransType(getCharacterDataFromElement(line));

            NodeList bankCode = element.getElementsByTagName("BankCode");
            line = (Element) bankCode.item(0);
            System.out.println("BankCode: " + getCharacterDataFromElement(line));
            cancelMandateResponse.setBankCode(getCharacterDataFromElement(line));

            NodeList billerID = element.getElementsByTagName("BillerID");
            line = (Element) billerID.item(0);
            System.out.println("BillerID: " + getCharacterDataFromElement(line));
            cancelMandateResponse.setBillerID(getCharacterDataFromElement(line));

            NodeList billerName = element.getElementsByTagName("BillerName");
            line = (Element) billerName.item(0);
            System.out.println("BillerName: " + getCharacterDataFromElement(line));
            cancelMandateResponse.setBillerName(getCharacterDataFromElement(line));

            NodeList billerTransId = element.getElementsByTagName("BillerTransId");
            line = (Element) billerTransId.item(0);
            System.out.println("BillerTransId: " + getCharacterDataFromElement(line));
            cancelMandateResponse.setBillerTransId(getCharacterDataFromElement(line));

            NodeList responseCode = element.getElementsByTagName("ResponseCode");
            line = (Element) responseCode.item(0);
            System.out.println("ResponseCode: " + getCharacterDataFromElement(line));
            cancelMandateResponse.setResponseCode(getCharacterDataFromElement(line));

        }

        return cancelMandateResponse;

    }

    private static String get_SHA_1_XML(String secretKey, String xml2) {
/*
        String generatedXML = null;
        byte[] xml = xml2.getBytes();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            md.update(xml);
            byte[] bytes = md.digest(secretKey.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedXML = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedXML;*/
        System.out.println(xml2);
        System.out.println(secretKey);
        String generatedPassword = null;
        xml2 = xml2 + secretKey;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            //md.update(secretKey.getBytes());
            byte[] bytes = md.digest(xml2.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++){
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        //System.out.println(generatedPassword);
        return generatedPassword;

    }

    private static String appendZerotoBankCode(String bankCode){

        if (bankCode.length() == 2){
            return "0"+bankCode;
        }
        else return bankCode;
    }


}
