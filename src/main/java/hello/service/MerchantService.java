package hello.service;


import hello.Repository.MerchantRepository;
import hello.Repository.TransactionRepository;
import hello.controller.Request.LoginRequest;
import hello.controller.Response.BarChatResponse;
import hello.controller.Response.LoginResponse;
import hello.controller.Response.StatisticsResponse;
import hello.controller.Response.TransactionReponse;
import hello.model.MerchantsEntity;

import hello.model.TransactionDetailsEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class MerchantService {

    public static final Logger logger = LoggerFactory.getLogger(MerchantService.class);

    private final Environment environment;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private TransactionRepository transactionRepository;


    @Autowired
    public MerchantService(Environment environment, EntityManager entityManager){
        this.environment = environment;
        this.entityManager = entityManager;

        entityManager.setFlushMode(FlushModeType.COMMIT);

    }

    public LoginResponse createLogin(LoginRequest loginRequest) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(loginRequest.getPassword().getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for (byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }
        System.out.println(sb.toString());
        System.out.println("Total " +merchantRepository.count());
        //logger.debug(sb.toString());
        MerchantsEntity merchant = merchantRepository.findMerchantsEntityByEmailAndPassword(loginRequest
                .getEmail(), sb.toString());
        System.out.println("Name" + merchantRepository.findMerchantsEntityByEmailAndPassword(loginRequest
                .getEmail(), sb.toString()));
        LoginResponse loginResponse =  new LoginResponse();
        loginResponse.setToken(merchant.getPassword());
        loginResponse.setBusinessName(merchant.getBusinessname());
        loginResponse.setEmail(merchant.getEmail());
        loginResponse.setMerchantId(merchant.getMerchantid());
        return loginResponse;
    }

    public StatisticsResponse statistics(String merchantId, LocalDateTime startDate, LocalDateTime endDate) throws Exception {
        List<BarChatResponse> barChatResponseList = new ArrayList<BarChatResponse>();
        //List<TransactionReponse> transactionReponseListSuccess = new ArrayList<TransactionReponse>();
        //List<TransactionReponse> transactionReponseListFailed = new ArrayList<TransactionReponse>();
        //List<TransactionReponse> transactionReponseListCaptured = new ArrayList<TransactionReponse>();
        List<TransactionDetailsEntity> trans= transactionRepository.findByMerchantIdAndCreatedAtBetween(merchantId, startDate, endDate);
        //List<TransactionDetailsEntity> transSuccess= transactionRepository.findByMerchantIdAndTransactionStatusAndCreatedAtBetween(merchantId,"success", startDate, endDate);
       // List<TransactionDetailsEntity> transFailed= transactionRepository.findByMerchantIdAndTransactionStatusAndCreatedAtBetween(merchantId,"failed", startDate, endDate);
       // List<TransactionDetailsEntity> transCaptured= transactionRepository.findByMerchantIdAndTransactionStatusAndCreatedAtBetween(merchantId,"CAPTURED", startDate, endDate);
        double revenue = 0;
        double success = 0;
        double failed = 0;
        double captured = 0;
        System.out.println("size " + trans.size());
        //System.out.println("size Failed" + transFailed.size());
        //System.out.println("size Captured" + transCaptured.size());
       // System.out.println("size" + transSuccess.size());

        for (TransactionDetailsEntity t: trans){
            if (t.getTransactionStatus().equalsIgnoreCase("success") ||
                    t.getTransactionStatus().equalsIgnoreCase("authorized") ||
                    t.getTransactionStatus().equalsIgnoreCase("captured"))
                success += Double.parseDouble(t.getAmount());
            else if (t.getTransactionStatus().equalsIgnoreCase("failed"))
                failed += Double.parseDouble(t.getAmount());
            else if (t.getTransactionStatus().equalsIgnoreCase("captured"))
                captured += Double.parseDouble(t.getAmount());
        }

        StatisticsResponse statisticsResponse = new StatisticsResponse();

        statisticsResponse.setCaptured(captured);
        statisticsResponse.setFailed(failed);
        statisticsResponse.setSuccess(success);
        statisticsResponse.setRevenue(success);
        statisticsResponse.setSettlement(success);

         success = 0;
         failed = 0;
         captured = 0;
        BarChatResponse barChatResponse;
        for (TransactionDetailsEntity t: trans){
            barChatResponse =  new BarChatResponse();
            if (t.getTransactionStatus().equalsIgnoreCase("success"))
                success = Double.parseDouble(t.getAmount());
            else if (t.getTransactionStatus().equalsIgnoreCase("failed"))
                failed = Double.parseDouble(t.getAmount());
            else if (t.getTransactionStatus().equalsIgnoreCase("captured"))
                captured = Double.parseDouble(t.getAmount());
            barChatResponse.setCaptured(captured);
            barChatResponse.setSuccess(success);
            barChatResponse.setFailed(failed);
            barChatResponse.setDate(t.getCreatedAt().toString());
            barChatResponseList.add(barChatResponse);
        }

        statisticsResponse.setBarChart(barChatResponseList);


        return statisticsResponse;
    }

    public List<TransactionReponse> transactions(String merchantId, LocalDateTime startDate, LocalDateTime endDate) throws Exception {

        List<TransactionReponse> transactionReponseList = new ArrayList<TransactionReponse>();

        List<TransactionDetailsEntity> trans= transactionRepository.findByMerchantIdAndCreatedAtBetween(merchantId, startDate, endDate);

        System.out.println("size " + trans.size());

        TransactionReponse transactionReponse;
        for (TransactionDetailsEntity t: trans){
            transactionReponse =  new TransactionReponse();
            transactionReponse.setDate(t.getCreatedAt().toString());
            transactionReponse.setStatus(t.getTransactionStatus());
            transactionReponse.setTransId(t.getTransId());
            transactionReponse.setNarration(t.getNarration());
            transactionReponse.setAmount(Double.parseDouble(t.getAmount()));
            transactionReponseList.add(transactionReponse);
        }
        return transactionReponseList;
    }


}
