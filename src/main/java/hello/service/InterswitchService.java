package hello.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import hello.controller.Response.TransactionNotificationResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
//import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;

import java.util.HashMap;
import java.util.Map;

@Service
public class InterswitchService {

    private final RestTemplate restTemplate;

    @Value("${mes.interswitchLoginURL}")
    private String interswitchLoginURL;

    @Value("${mes.interswitchGenerateQrURL}")
    private String interswitchGenerateQrURL;

    @Value("${mes.interswitchTransactionStatusURL}")
    private String interswitchTransactionStatusURL;

    @Value("${mes.username}")
    private String username;

    @Value("${mes.password}")
    private String password;

    @Value("${mes.owner}")
    private String owner;

    @Value("${mes.qr}")
    private String qr;

    @Value("${mes.type}")
    private String type;

    @Value("${mes.currencyCode}")
    private String currencyCode;

    @Value("${mes.cityName}")
    private String cityName;

    @Value("${mes.country}")
    private String country;

    @Value("${mes.createQRTransaction}")
    private String createQRTransaction;

    public InterswitchService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public String getToken()  {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = new HashMap<String, String>();
        JSONObject body = new JSONObject();

        System.out.println(interswitchLoginURL);
        try {
            body.put("username", username);
            body.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestBody = new HttpEntity<String>(body.toString(), headers);
        // Send request with POST method.
        ResponseEntity<String> result  = restTemplate.exchange(
                interswitchLoginURL, HttpMethod.POST, requestBody, String.class);
        //System.out.println(result.getBody());
        String[] values = result.getBody().split("\"data\":");

        //System.out.println(values[1].substring(0, values[1].length()-1));
        //System.out.println(Arrays.toString(values));
        if ( result.getStatusCode() == HttpStatus.OK){
            // convert JSON string to Map
            try {
                map = mapper.readValue(values[1].substring(0, values[1].length()-1), new TypeReference<Map<String, String>>(){});
                //System.out.println(map);
                System.out.println(map.get("access_token"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map.get("access_token");

    }

    /*public String generateQrImage(String amount, String transRef)  {

        *//*final String CONSTANT = org.apache.commons.lang3.StringUtils.join( new String[] {
                "This string is long",
                "really long...",
                "really, really LONG!!!"
        } );*//*
        return generateQrCode(amount, transRef);
    }*/

    public byte[] generateQrImage(String amount, String transRef, String merchantName)  {

        //final String encodedString = StringUtils.join( new String[] { generateQrCode(amount, transRef)} );
        //byte[] decodedBytes = Base64.getDecoder().decode(StringUtils.join( new String[] { generateQrCode(amount, transRef)} ));


        byte[] decodedBytes = Base64Utils.decode(generateQrCode(amount, transRef, merchantName).getBytes());

        return decodedBytes;
    }

    public String generateQrCode(String amount, String transRef, String merchantName)  {

        String token = getToken();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = new HashMap<String, String>();
        JSONObject body = new JSONObject();

        try {
            body.put("owner", owner);
            body.put("type", type);
            body.put("cityName", cityName);
            body.put("country", country);
            body.put("currencyCode", currencyCode);
            body.put("createQRTransaction", createQRTransaction);
            body.put("transactionReference", transRef);
            body.put("businessName", merchantName);
            body.put("amount", amount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println(interswitchGenerateQrURL);
        System.out.println(body.toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + token);
        HttpEntity<String> requestBody = new HttpEntity<String>(body.toString(), headers);
        // Send request with POST method.
        ResponseEntity<String> result  = restTemplate.exchange(
                interswitchGenerateQrURL, HttpMethod.POST, requestBody, String.class);
        //System.out.println(result.getBody());
        System.out.println(result.getStatusCode());
        if ( result.getStatusCode() == HttpStatus.CREATED){
            // convert JSON string to Map
            try {
                map = mapper.readValue(result.getBody(), new TypeReference<Map<String, String>>(){});
                //System.out.println(map);
                System.out.println(map.get("qrCodeData"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map.get("qrCodeData");
    }


    public TransactionNotificationResponse getTransacctionNotification(String transRef)  {

        String token = getToken();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = new HashMap<String, String>();
        //JSONObject body = new JSONObject();

        String url = interswitchTransactionStatusURL + transRef + "&merchantCode=" + owner;
        System.out.println("Url" + url);
        System.out.println("Transaction Ref" + transRef);
        //System.out.println(body.toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + token);
        HttpEntity<String> requestBody = new HttpEntity<String>(headers);
        // Send request with POST method.
        ResponseEntity<String> result  = restTemplate.exchange(
                url, HttpMethod.GET, requestBody, String.class);
        System.out.println(result.getBody());
        System.out.println(result.getStatusCode());
        if ( result.getStatusCode() == HttpStatus.OK){
            // convert JSON string to Map
            try {
                map = mapper.readValue(result.getBody(), new TypeReference<Map<String, String>>(){});
                System.out.println(map);
                //System.out.println(map.get("responseCode"));
                //System.out.println(map.get("transactionReference"));
                //System.out.println(map.get("responseDescription"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        TransactionNotificationResponse transactionNotificationResponse = new TransactionNotificationResponse(
                map.get("responseCode"), map.get("responseDescription"), map.get("transactionReference"),
                map.get("merchantCustomerName"), map.get("amount")
        );
        return transactionNotificationResponse;
    }

}
