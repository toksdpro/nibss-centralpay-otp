package hello.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@Data
//@EqualsAndHashCode(callSuper = true)
@Table(name = "merchant")
public class MerchantsEntity {

    @Id
    @Column(name = "id", nullable = false)
    private int id;
    private String businessname;
    private String fullname;
    private String email;
    private String phone;
    private Integer paymentMethod;
    private Integer bankid;
    private Byte useMpgs;
    private String testMerchantid;
    private String merchantid;
    private Byte usepreauth;
    private String accountno;
    private String password;
    private String website;
    private String testKey;
    private String secretKey;
    private byte live;
    private String gateway;
    private byte applyMastercardDiscount;
    private byte confirmed;
    private Timestamp dateregistered;
    private byte useinternetbanking;
    private String status;


}
