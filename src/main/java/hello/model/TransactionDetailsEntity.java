package hello.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@Data
//@EqualsAndHashCode(callSuper = true)
@Table(name = "transaction_details")
public class TransactionDetailsEntity {

    @Id
    @Column(name = "id", nullable = false)
    private int id;
    private String transId;
    private String orderId;
    private String fullName;
    private String narration;
    private String amount;
    private String email;
    private LocalDateTime orderTime;
    private String merchantId;
    private String bankId;
    private String gateway;
    private String transactionStatus;
    private LocalDateTime createdAt;
    private String pan;
    private double discount;
    private String currency;
    private String responseData;
    private String acqCode;
    private String masterCardPan;

}
