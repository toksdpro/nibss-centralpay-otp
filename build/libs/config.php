<?php


return [
    "MPGS" =>[
        "pay" => [
            "merchantID" => "3214LAFCMB2J",
            "password"   => "4892d3e2f2237e03364ec276118669a8",
            "password_test" => "db2eae2a093c588b360595d5a8001fe6"
        ],
        "preauth" =>[
            "merchantID" => "3214LAFCMB2K",
            "password"   => "20ace6b6cb4703b0290236fe5bce975e",
            "password_test" => "db2eae2a093c588b360595d5a8001fe6"
        ]		
    ],
    "Database" => [
        "username" => "root",
        "password" => "Password12$",
        "database" => "netpluspay",
        "dsn"      => "mysql:host=localhost",
        "options"  => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    ],
    "Currency" => [
        "NGN" => "&#8358;",
        "USD" => "&#36;",
        "GBP" => "&#163;",
        "EUR" => "&#8364;",
    ],
    "URLS" => [
        "pay" => [
            "prod" => "https://fcmb.gateway.mastercard.com/api/rest/version/41/merchant/3214LAFCMB2J/",
            "test" => "https://test-fcmb.gateway.mastercard.com/api/rest/version/41/merchant/3214LAFCMB2J/",
        ],
        "preauth" => [
            "prod" => "https://fcmb.gateway.mastercard.com/api/rest/version/41/merchant/3214LAFCMB2K/",
            "test" => "https://test-fcmb.gateway.mastercard.com/api/rest/version/41/merchant/3214LAFCMB2K/",
        ]	
    ],
    "MIGS" => [
        "access_code" => "B84ACCE8",
        "merchant_id" => "TESTGTB800668C02",
        "secure_secret" => "5F6E719AF9E76796FE313ECCDC6FDDC2",
        "return_url"  => "https://netpluspay.com/testpayment/class/fcmb_response"
    ]
];