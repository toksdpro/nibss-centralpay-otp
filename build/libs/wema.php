<?php
/**
 * Created by PhpStorm.
 * User: May
 * Date: 26/10/2015
 * Time: 17:59
 */

session_start();
require '../../class/payment.php';

class wema extends Payment {
    public $merchant_id;
    public $full_name;
    public $total_amount;
    public $narration;
    public $order_time;
    public $currency_code;
    public $merchant_name;
    public $merchant_website;
    public $trans_id;
    public $email;
    public $return_url;
    public $order_id;
    public $warning;
    public $accounts;
    public $call_url = 'https://apps.wemabank.com/remitanaps/wemaribredirectLive.aspx?netpluspay_transId=';
    //$call_url = 'http://172.27.4.135/wemaonlinenewuitest/default.aspx?netpluspay_transId='.$payment->trans_id;
    //$call_url = 'http://196.43.215.83/wemaonlinenewuitest/default.aspx?netpluspay_transId='.$payment->trans_id;

    public function log_request($data, $url)
    {
        try {
            $stmt_add_log = $this->dbh->prepare("INSERT INTO logs (transaction_id, `call`, `url`, request_data, response_data, response_code) VALUES (:trans_id, 'Wema Request', :url, :request_data, '', '')");
            $stmt_add_log->bindParam(':trans_id', $data['trans_id']);
            $stmt_add_log->bindParam(':url', $url);
            $stmt_add_log->bindParam(':request_data', json_encode($data));
            $stmt_add_log->execute();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }
	    public function retrieve_request($trans_id)
    {
        try {
            $stmt = $this->dbh->prepare("SELECT * FROM logs WHERE transaction_id = :transaction_id AND `call` = 'Wema Request'");
            $stmt->bindParam(':transaction_id', $trans_id);
            $stmt->execute();
            return $log_data = $stmt->fetchAll();

        } catch (PDOException $e) {
            return '-1';
        }

    }
	 public function update_request($trans_id,$data)
    {
        try {
            $stmt = $this->dbh->prepare("UPDATE logs SET response_data = :response_data, call_type = 'T' WHERE transaction_id = :transaction_id AND `call` = 'Wema Request'");
            $stmt->bindParam(':response_data', $data);
            $stmt->bindParam(':transaction_id', $trans_id);
            $stmt->execute();
        } catch (PDOException $e) {

        }

    }

    public function update_transaction($trans_id,$status)
    {
        try {
            $stmt = $this->dbh->prepare("UPDATE transaction_details SET transaction_status = :status WHERE trans_id = :transaction_id");
            $stmt->bindParam(':transaction_id', $trans_id);
            $stmt->bindParam(':status', $status);
            $stmt->execute();
        } catch (PDOException $e) {

        }

    }

}