<?php
session_start();

require "class/payment.php";
$payment = new payment;
$response = "";
$payment->bank(NULL);

if (isset($_REQUEST['bank'])) {
    $payment->bank($_REQUEST);
}

if (isset($_GET['action']) && $_GET['action'] == "cancel_trans") {
    $payment->logout('');
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Netpluspay</title>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/select.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Signika+Negative' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>


<!-- Vendor libraries -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">


<!-- If you're using Stripe for payments -->



<style>

.credit-card-div  span {
    padding-top:10px;
        }
.credit-card-div img {
    /*padding-top:30px;*/
}
.credit-card-div .small-font {
    font-size:9px;
}
.credit-card-div .pad-adjust {
    padding-top:10px;
}

.bnk div, .bnk{
    background-color: #fafafa;
}

#bank-pick {
    background-color: #F9F9F9;
}

#bank-pick:hover {
    background-color: #f2f2f2;
}

#bank-pick:focus, #bank-pick:active:focus {
    background-color: #f2f2f2;
}

#card-pick {
    background-color: #F9F9F9;
}

#card-pick:hover {
    background-color: #f2f2f2;
}

#card-pick:focus, #card-pick:active:focus {
    background-color: #f2f2f2;
}

</style>

<script data-rocketsrc="http://www.designbootstrap.com/track/ga.js"  type="text/rocketscript"></script>

</head>
<body>

<section id="payment">
    <div class="container">
        <div class="row ">
            <div class="col-md-4 col-md-offset-4">
                <div class="credit-card-div">
                    <div class="panel panel-default" >
                        <div class="panel-heading">

                        

                            <div class="row" style="border-bottom: 1px solid #ddd;">
                                <div class="col-md-3 col-sm-4 col-xs-4">
                                    <img style="padding-top:0 !important; margin-bottom:10px;" class="img-rounded" src="assets/images/net2.png">
                                </div>

                                <div class="col-md-8 col-sm-8 col-xs-8" align="left" style="padding-left:0 !important;">
                                    <span class="info text-muted " > <?php echo $payment->full_name ?></span>
                                    <span style="color:#f45b5b;" class="info text-muted " > &#8358; <?php echo number_format($payment->total_amount,2)?></span>
                                </div>
                            </div>
                            <span align='center' class="help-block text-muted" style="text-align:center;">Select Payment type </span>
                            <div class="row" style="border-bottom: 2px solid #ff4000; border-top:1px solid #ddd;">
                                <!-- <div id="card-pick" class="col-md-6 col-sm-6 col-xs-6" style="padding:10px;" align="center">
                                    
                                    <input type="hidden" name="migs" value="migs"/>
                                    <input style="border:none; background-color:#fafafa;" class="btn btn-default" type="submit" value="CARD PAYMENT">
                                    </form>
                                </div>
 -->
                                <div id="bank-pick" class="col-md-6 col-sm-6 col-xs-6" style="padding:10px; border-left:1px solid #ddd;" align="center">
                                    <button style="border:none; background-color:#fafafa;" class="btn btn-default" type="button" value="INTERNET BANKING">INTERNET BANKING</button>
                                </div>
                            </div>
                      
                      
                             <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" >
                             <span id="bank" style="display: none;">
                             <span align='center' class="help-block text-muted " >Select Preferred Bank</span>
                             <div class="bank">
                             
                                 <?php
                                    $stmt = $payment->dbh->prepare("SELECT * FROM banks WHERE status = 'Active'");
                                    $stmt->execute();
                                    $banks = $stmt->fetchAll();

                                    for($i = 0; $i <= count($banks) -1; $i++) {
                                ?>                               
                                 <a href="">
                                 <div class="row bnk" style="padding: 10px; border-bottom:1px solid #ddd;">
                                     <div class="col-md-3 col-sm-3 col-xs-3">
                                        <img class="img-responsive"
                                         src="<?php echo $payment->base_url . "assets/images/"; ?><?php echo $banks[$i]['image']; ?>" 
                                         alt="<?php echo $banks[$i]['bank_name'] ?>" width="45px">
                                    </div>

                                     <div class="col-md-6 col-sm-6 col-xs-6">
                                    <label class="help-block text-muted"> 
                                        <input type="radio" class="radio" name="bank" id="<?php echo $banks[$i]['bank_id'] ?>" 
                                        value="<?php echo $banks[$i]['bank_id'] ?>" /><?php echo $banks[$i]['bank_name'] ?>
                                    </label>
                                         
                                     </div>

                                     <div class="col-md-3" align="right">
                                         <i style="padding-top: 18px;" class="fa fa-caret-right" aria-hidden="true"></i>
                                     </div>
                                 </div></a>
                                     <?php
                            }
                            ?>

                                 
                            </div>
                            </span>

                            <div class="row" style="padding: 10px;">
                                <div class="col-md-12 col-sm-12 col-xs-12 pad-adjust">
                                    <input disabled id="proceed" type="submit"  class="btn btn-warning btn-block" value="PAY NOW"  />
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                    <h6 style="color:#fff;" class="text-center">NetPlusPay By <a href="http://www.netplusdotcom.com" target="_blank">NetPlusDotCom</a> </h6>
                </div>
           </div>
        </div>
    </div>  
</section>
    <!-- CONATINER END -->


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.13.1/jquery.validate.min.js"></script>  
    <script src="assets/js/bridge.js"></script>

    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>


    <script type="text/javascript">

     function ccv(entry){
      return (entry.val().length == 3);
     }

        (function($){
            $(function(){


                  // $('#card').bridge_validate('#proceed');

                  
                  // $('#card-pick').bridge('#card','click',function(){
                  //   this.show();
                  // });

                  // $('#card-pick').bridge('#bank','click',function(){
                  //   this.hide();
                  // });


                  // $('#bank-pick').bridge('#card','click',function(){
                  //   this.hide();
                  // });


                  $('#bank-pick').bridge('#bank','click',function(){
                    this.show();
                  });


                  $('.bnk').bridge('#proceed','click',function(){
                    //this.css('background-color','#00ff00');
                    this.removeAttr('disabled');

                  });

                  function reset(){
                    $('.bnk').each(function(){
                        $(this).removeClass('picked'); //('background-color','#fafafa');
                    });
                  }

                  $('.bnk').bridge('#proceed','click',function($target,$sender){
                    

                    reset();

                    $sender.find('input').each(function(){
                      $(this).prop('checked',true)
                    });

                    //$sender.css('background-color','#f2f2f2');
                    $sender.addClass('picked');

                  });


                  // $('.bnk').bridge('#proceed','mouseout',function(){
                  //   this.css('background-color','#ff0000');
                  // });


            });
        })(jQuery);
    </script>



  </body>
</html>
