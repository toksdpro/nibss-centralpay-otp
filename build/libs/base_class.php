<?php

class Base_class
{
    private $hostname = "NetPlusPayDB.db.8109290.hostedresource.com";
    private $username = "NetPlusPayDB";
    private $password = "asdda324H@";
    private $db_link;
    private $dbname = "NetPlusPayDB";
    private $token_key = "a245892c67526624e478375678b43578f365092cba435893465784358463558b";
    private $delimiter = "<--BREAKINGPOINTFORTOKEN-->";
    public $dbh;
    public $base_url;

    public $success = '00';
    public $error_missing_field = '01';
    public $error_unknown_merchant_id = '21';
    public $error_unknown_calling_domain_field = '22';
    public $error_unknown_response_domain_field = '23';
    public $error_incorrect_signature_field = '40';
    public $error_transaction_cancelled = '50';
    public $error_general = '90';
    public $error_no_response_from_gateway = '92';
    public $error_no_duplicate_transaction = '93';

    public $fidelity_account = '4010962858';
    public $fcmb_account = '2684362013';

    public function __construct()

    {
        error_reporting(0);
        session_start();
        if ($_SERVER['HTTP_HOST'] == 'localhost') {
            $this->base_url = 'http://localhost/netpluspay/payment/';
            $user = 'root';
            $pass = 'presario';
            $this->dbh = new PDO('mysql:host=localhost;dbname=netpluspaytest',$user, $pass);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } else if ($_SERVER['HTTP_HOST'] == 'webmallnglab.com') {
            $this->base_url = 'http://webmallnglab.com/netpluspay/payment/';
            $user = 'NetPlusPayDB';
            $pass = 'asdda324H@';
            $this->dbh = new PDO('mysql:host=NetPlusPayDB.db.8109290.hostedresource.com;dbname=NetPlusPayDB', $user, $pass);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } else {
            $this->base_url = 'https://netpluspay.com/payment/';
            $user = 'root';
            $pass = 'Password12$';
            $this->dbh = new PDO('mysql:host=localhost;dbname=netpluspay', $user, $pass);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }

    public function filter_data($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function form_validation($data = "", $require = "", $type = "", $type2 = "")
    {
        if ($require == "REQUIRE") {
            if (empty($data)) {
                $data = "NULL";
            }
        }

        if ($data == "NULL") {
            return $data;
        }

        if ($type == "ALPHABETICAL") {
            if (!preg_match("/^[a-zA-Z ]*$/", $data)) {
                $data = "NOT_ALPHABETICAL";
            }

        }

        if ($type == "VALID-EMAIL") {
            if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $data)) {
                $data = "INVALID_EMAIL";
            }
        }

        if ($type == "WEBSITE") {
            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $data)) {
                $data = "INVALID_WEBSITE";
            }
        }

        if ($type == "ALPHA_NUMERIC") {
            if (!preg_match("/^[a-zA-Z 0-9]*$/", $data)) {
                $data = "NOT_ALPHA_NUMERIC";
            }
        }

        if ($type == "NUMERIC") {
            if (!preg_match("/^[0-9]*$/", $data)) {
                $data = "NOT_NUMERIC";
            }
        }

        if ($type2 == "LENGTH-10") {
            if (strlen($data) != 10) {
                $data = "INVALID_LENGTH";
            }
        }
        return $data;
    }

    protected function encrypt_data($encrypt)
    {
        $key = $this->token_key;
        $encrypt = serialize($encrypt);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
        $key = pack('H*', $key);
        $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt . $mac, MCRYPT_MODE_CBC, $iv);
        $encoded = base64_encode($passcrypt) . '|' . base64_encode($iv);
        return $encoded;
    }

    public function decrypt_data($decrypt)
    {
        $key = $this->token_key;
        $decrypt = explode('|', $decrypt);
        $decoded = base64_decode($decrypt[0]);
        $iv = base64_decode($decrypt[1]);
        $key = pack('H*', $key);
        $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
        $mac = substr($decrypted, -64);
        $decrypted = substr($decrypted, 0, -64);
        $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));

        if ($calcmac !== $mac) {
            return false;
        }

        $decrypted = unserialize($decrypted);
        return $decrypted;
    }

    protected function random_alphabet()
    {
        $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $rand_string = $characters[rand(0, strlen($characters) - 1)];
        return $rand_string;
    }


//    protected function bank_auth_check($token)
//    {
//        if (!empty($token)) {
//            if (isset($_SESSION['fcmb_bank_user_token']) && !empty($_SESSION['fcmb_bank_user_token'])) {
//                if ($_SESSION['fcmb_bank_user_token'] == $token) {
//                    $user_details = $this->get_token_data($token);
//                    if (count($user_details) > 1) {
//                        return true;
//                    } else {
//                        return false;
//                    }
//                } else {
//                    return false;
//                }
//            } else {
//                return false;
//            }
//        } else {
//            return false;
//        }
//    }
//
//    protected function request_authentication_api($url)
//    {
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_HEADER, false);
//        curl_setopt($ch, CURLOPT_URL, $url);
//        $result = curl_exec($ch);
//        curl_close($ch);
//        return $result;
//    }
//
//    protected function request_debit_api($url)
//    {
//        $postdata = "";
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
//        $result = curl_exec($ch);
//        curl_close($ch);
//        return $result;
//    }


    public function transaction_notification($email,$amount,$bank,$transaction_id,$merchant_name,$order_number,$status)
    {
        $mail_to = $email;
        $subject = "NetplusPay Payment Notification";
        $from = 'noreply@netpluspay.com';
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From:' . $from . "\r\n";

        $message = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>NetplusPay notification</title>
<style type="text/css">
<!--
.style6 {
        color: #000066;
        font-weight: bold;
}
-->
</style>
</head>

<body>
<table bgcolor="#fff" align="center" width="600" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="600" height="70" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="600" height="70" valign="top"><table bgcolor="#749A20" width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="248" rowspan="3" valign="top"><img style="padding: 10px" src="http://netpluspay.com/images/email/logo.jpg" width="234" height="69" /></td>
            <td width="119" height="29">&nbsp;</td>
            <td width="208">&nbsp;</td>
            <td width="19">&nbsp;</td>
          </tr>
          <tr>
            <td height="24">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>        </td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td height="108" valign="top" style="padding-left:20px; padding-top:10px"><p style="font-family:Geneva, Arial, Helvetica, sans-serif; color:#666666; font-size:16px"><b>Thank you!<br/><br/> Your payment from '.ucfirst($merchant_name).' <br/> Status: '.$status.'.</b><br/>See details below</p></td>
  </tr>
  <tr>
    <td height="121" valign="top"><table bgcolor="#EFF3F7" width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="63" colspan="3" valign="top"><table  bgcolor="#749A20" width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="24" height="20">&nbsp;</td>
                <td width="205">&nbsp;</td>
                <td width="34">&nbsp;</td>
              </tr>
          <tr>
            <td height="34"></td>
                <td valign="top" style=" font-family:Geneva, Arial, Helvetica, sans-serif;text-align:center; color:#FFFFFF"><b>Order ID </b></td>
                <td></td>
              </tr>
          <tr>
            <td height="9"></td>
                <td></td>
                <td></td>
              </tr>
          </table></td>
          <td width="4">&nbsp;</td>
          <td width="189" valign="top "><table  bgcolor="#749A20" width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="189" height="23">&nbsp;</td>
              </tr>
              <tr>
                <td height="30" valign="top" style=" font-family:Geneva, Arial, Helvetica, sans-serif;text-align:center; color:#FFFFFF"><b>Bank</b></td>
              </tr>
              <tr>
                <td height="10"></td>
              </tr>
          </table></td>
          <td width="4">&nbsp;</td>
          <td width="140" valign="top"><table  bgcolor="#749A20" width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="140" height="22">&nbsp;</td>
              </tr>
              <tr>
                <td height="29" valign="top" style=" font-family:Geneva, Arial, Helvetica, sans-serif;text-align:center; color:#FFFFFF"><b>Amount</b></td>
              </tr>
              <tr>
                <td height="12"></td>
              </tr>
          </table></td>
        </tr>
      <tr>
        <td width="26" height="20">&nbsp;</td>
          <td width="201">&nbsp;</td>
          <td width="36">&nbsp;</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      <tr>
        <td height="32">&nbsp;</td>
          <td valign="top"><div align="center"  style=" font-family:Geneva, Arial, Helvetica, sans-serif;text-align:center; color:#FF0000"><b>'.$order_number.'</b></div></td>
          <td>&nbsp;</td>
          <td></td>
          <td valign="top"><div align="center" style=" font-family:Geneva, Arial, Helvetica, sans-serif;text-align:center;">'.$bank.'</div></td>
          <td></td>
          <td valign="top"><div align="center" style=" font-family:Geneva, Arial, Helvetica, sans-serif;text-align:center;"><strong>&#8358;</strong> &#x20A6; '.number_format($amount,2).'</div></td>
        </tr>
      <tr>
        <td height="6"></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td height="94" valign="top" bg><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="56" height="14"></td>
        <td width="267"></td>
        <td width="49"></td>
        <td width="50"></td>
        <td width="4"></td>
        <td width="174"></td>
      </tr>
      <tr>
        <td rowspan="2" valign="top"><img style=" padding-top:10px; margin-left:35px;" src="http://netpluspay.com/images/email/note.png" width="20" height="20" />&nbsp;</td>
        <td rowspan="3" valign="top" style=" font-family:Geneva, Arial, Helvetica, sans-serif; font-size:13px; padding-top:15px;"><strong>TRANSACTION_ID:</strong><span class="style6"> '.$transaction_id.'</span> </td>
        <td height="2"></td>
        <td></td>
        <td></td>
        <td rowspan="3" valign="top" style=" font-family:Geneva, Arial, Helvetica, sans-serif; font-size:13px; padding-top:15px;"><strong>DATE:</strong> <span class="style6">'.date("%M %D, %Y %H:%i:%s").'</span></td>
      </tr>
      <tr>
        <td height="44"></td>
        <td valign="top"><img style=" padding-top:10px; margin-left:25px;" src="http://netpluspay.com/images/email/cal.png" width="20" height="20" />&nbsp;</td>
        <td></td>
      </tr>
      <tr>
        <td height="3"></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td height="36"></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td height="34">&nbsp;</td>
  </tr>
  <tr>
    <td height="193" valign="top"><table bgcolor="#FFFFFF" width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="600" height="165" valign="top" style=" line-height:em; font-size:14px; font-family:Geneva, Arial, Helvetica, sans-serif; text-align:center; padding-top:20px;">
          <p><strong>Got questions to ask?</strong><br />
            <strong>Contact us via:</strong> <a href="mailto:support@netpluspay.com">support@netpluspay.com</a></p>          </td>
      </tr>
      <tr>
        <td height="28">&nbsp;</td>
      </tr>
    </table>    </td>
  </tr>
</table>
</body>
</html>';


            mail($mail_to, $subject, $message, $headers);
    }
}
?>