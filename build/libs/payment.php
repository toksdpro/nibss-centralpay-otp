<?php
require 'base_class.php';

/**
 * NetPlusPay
 * @author        GeeksPerHr.com
 * @copyright    Copyrights(c) reserved at GeeksPerHr.com
 * @since        15-june-2014
 * @developer    Shobhit Singh modified by Mayowa Anibaba 10-2015
 */
// ------------------------------------------------------------------------
/**
 * Payment class Extended to Base_class for basic operation of this class.
 *
 * This class operates basic and advanced logic for NetPlusPay.
 * Remember to change base url defined in base_class(class extended to this class) if migrating to other domain.
 * And also database credentials are defined at base_class.
 */
session_start();
class Payment extends Base_class
{
    public $merchant_id;
    public $full_name;
    public $total_amount;
    public $narration;
    public $currency_code;
    public $merchant_name;
    public $merchant_website;
    public $email;
    public $return_url;
    public $order_id;
    public $order_time;
    public $trans_id;
    public $warning;
    public $error_code;
    public $field_error;
    public $bank_option;
    public $secretKey; 
    public $migs = false;
    public $preauth = true;

    public function check_order_request()
    {
        if (isset($_SESSION['order_request']['merchant_id']) && !empty($_SESSION['order_request']['merchant_id'])) {
        } else {
            $this->logout('');
            return true;
        }
        return false;
    }

    public function selectBank($data){
        header("location:select_bank.php");
        exit();
    }

    public function bank($data)
    {
        error_log("GOT here for bank again and bank is ".$data['bank']);  
        

        if (isset($data['bank'])) {
             


            if ($data['bank'] == 'NPPFCMB') {
                header("location:" . $this->base_url . "gate/nppfcmb/index.php");
                exit;
            } elseif ($data['bank'] == 'NPPFBN') {
                header("location:" . $this->base_url . "gate/nppfbn/index.php");
                exit;
            } elseif ($data['bank'] == 'NPPSB') {
                header("location:" . $this->base_url . "gate/nppsb/index.php");
                exit;
            } elseif ($data['bank'] == 'NPPGTB') {
                header("location:" . $this->base_url . "gate/nppgtb/gtb_redirect.php");
                exit;
            } elseif ($data['bank'] == 'NPPWB') {
				//var_dump($data);
				//die;
                header("location:" . $this->base_url . "gate/nppwb/wema_redirect.php");
                exit;
            } elseif ($data['bank'] == 'NPPUBA') {
                header("location:" . $this->base_url . "gate/nppuba/uba_redirect.php");
                exit;
            } elseif ($data['bank'] == 'NPPFB') {
                header("location:" . $this->base_url . "gate/nppfb/index.php");
                exit;
            } elseif ($data['bank'] == 'NPPHB') {
                header("location:" . $this->base_url . "gate/npphb/heritage_redirect.php");
                exit;
            } else {
                header("location:https://netpluspay.com/payment/select_bank");
                exit;
            }
        }
    }

    public function order_request($data)
    {
        var_dump('GOT Here');
        die;
        
        $merchant_id = $this->filter_data($data['merchant_id']);
        $merchant_id = $this->form_validation($merchant_id, "REQUIRE");

        $full_name = $this->filter_data($data['full_name']);
        $full_name = $this->form_validation($full_name, "REQUIRE");

        $total_amount = $this->filter_data($data['total_amount']);
        $total_amount = $this->form_validation($total_amount, "REQUIRE");

        $narration = $this->filter_data($data['narration']);
        $narration = $this->form_validation($narration, "REQUIRED");

        $currency_code = $this->filter_data($data['currency_code']);
        $currency_code = $this->form_validation($currency_code, "REQUIRED");

        $order_id = $this->filter_data($data['order_id']);
        $order_id = $this->form_validation($order_id, "REQUIRED");

        $email = $this->filter_data($data['email']);
        $email = $this->form_validation($email, "REQUIRE");

        $return_url = $this->filter_data($data['return_url']);
        $return_url = $this->form_validation($return_url, "REQUIRE", "WEBSITE");

        /********optional*********/

        if (isset($data['bank'])) {
            $bank = $this->filter_data($data['bank']);
        } else {
            $bank = 'All';
        }


        $valid_error = false;
        $this->field_error = '';
        if ($merchant_id == "NULL") {
            $valid_error = true;
            $this->field_error = 'Merchant ID';
            $this->error_code = $this->error_missing_field;
        }
        if ($full_name == "NULL") {
            $valid_error = true;
            $this->field_error = 'Full name';
            $this->error_code = $this->error_missing_field;
        }
        if ($total_amount == "NULL") {
            $valid_error = true;
            $this->field_error = 'Total Amount';
            $this->error_code = $this->error_missing_field;
        }
        if ($email == "NULL") {
            $valid_error = true;
            $this->field_error = 'Email';
            $this->error_code = $this->error_missing_field;
        }
        if ($order_id == "NULL") {
            $valid_error = true;
            $this->field_error = 'Order Id';
            $this->error_code = $this->error_missing_field;
        }
        if ($currency_code == "NULL") {
            $valid_error = true;
            $this->field_error = 'Currency Code';
            $this->error_code = $this->error_missing_field;
        }
        if ($narration == "NULL") {
            $valid_error = true;
            $this->field_error = 'Narration';
            $this->error_code = $this->error_missing_field;
        }
        if ($return_url == "NULL") {
            $valid_error = true;
            $this->field_error = 'Return URL';
            $this->error_code = $this->error_missing_field;
        }
        if ($return_url == "INVALID_WEBSITE") {
            $valid_error = true;
            $this->field_error = 'Invalid return URL';
            $this->error_code = $this->error_missing_field;
        }

        $this->order_id = $order_id;

        try {
            $stmt = $this->dbh->prepare("SELECT id, merchantid,  use_mpgs, usepreauth, useinternetbanking FROM merchant WHERE merchantid = :merchant_id");
            $stmt->bindParam(':merchant_id', $merchant_id);
            $stmt->execute();
            $merchant_data = $stmt->fetchAll();

            if ($merchant_data[0]['id'] < 1) {
                $valid_error = true;
                $this->field_error = 'Unknown merchant ID';
                $this->error_code = $this->error_unknown_merchant_id;
            }else{
                $migs_mid = $merchant_data[0]['merchantid'];
                $migs = $merchant_data[0]['use_mpgs'];
                $migs_data = true;
                $preauth = $merchant_data[0]['usepreauth'];
                $internet = $merchant_data[0]['useinternetbanking'];
            }
            $stmt = $this->dbh->prepare("SELECT order_id, merchant_id FROM transaction_details WHERE merchant_id = :merchant_id
                                         AND order_id = :order_id");
            $stmt->bindParam(':merchant_id', $merchant_id);
            $stmt->bindParam(':order_id', $order_id);
            $stmt->execute();
            $merchant_data = $stmt->fetchAll();
            if($merchant_data)
            {
                $valid_error = true;
                $this->field_error = 'Duplicate Transaction';
                $this->error_code = $this->error_no_duplicate_transaction;
            }

        } catch (PDOException $e) {
            // "Error: " . $e->getMessage();
            $valid_error = true;
            $this->field_error = $e->getMessage();
            $this->error_code = $this->error_general;
        }

        $check_url = explode("/", $_SERVER['HTTP_REFERER']);
        $check_notify_url = explode("/", $return_url);

        if (!isset($check_url[2]) && !isset($check_notify_url[2])) {
            $valid_error = true;
            $this->field_error = "Return URL not Valid";
        } elseif (isset($check_url[2]) && isset($check_notify_url[2])) {
            $final_check_domain = $check_notify_url[2];
            $final_domain = $check_url[2];

            if (substr($check_url[2], 0, 4) == "www.") {
                if (substr($check_notify_url[2], 0, 4) != "www.") {
                    $final_check_domain = 'www.' . $check_notify_url[2];
                }
            } else {
                if (substr($check_notify_url[2], 0, 4) == "www.") {
                    $final_domain = 'www.' . $check_url[2];
                }
            }

            if ($final_domain != $final_check_domain) {
                $valid_error = true;
                $this->field_error = "Host and return URLs don't match";
                $this->error_code = $this->error_unknown_response_domain_field;
            }
        }

        if ($valid_error === false) {
            $my_array = array();
            $my_array['merchant_id'] = $this->encrypt_data($merchant_id);
            $my_array['full_name'] = $this->encrypt_data($full_name);
            $my_array['total_amount'] = $this->encrypt_data($total_amount);
            $my_array['currency_code'] = $this->encrypt_data($currency_code);
            $my_array['email'] = $this->encrypt_data($email);
            $my_array['narration'] = $this->encrypt_data($narration);
            $my_array['order_id'] = $this->encrypt_data($order_id);
            $my_array['return_url'] = $this->encrypt_data($return_url);
            $my_array['trans_id'] = $this->encrypt_data('NP' . date('ymdhisu') . rand(100, 999));
            $my_array['bank'] = $this->encrypt_data($bank);

            $this->trans_id = $this->decrypt_data($my_array['trans_id']);
            $url = $return_url;

            $_SESSION['order_request'] = $my_array;
            $user_data['order_request'] = $data;
            $_SESSION['usepreauth'] = $preauth;
            $_SESSION['useib'] = $internet;
            $_SESSION['use_mpgs'] = $migs;
            $fields = $user_data;
            $fields_string = http_build_query($fields);

            try {
                $trans_id = $this->decrypt_data($my_array['trans_id']);
                $request_data = json_encode($my_array);
                $stmt_add_log = $this->dbh->prepare("INSERT INTO logs (transaction_id, `call`, `url`, request_data, response_data, response_code) VALUES (:trans_id, 'Incoming request', '', :request_data, '', '')");
                $stmt_add_log->bindParam(':trans_id', $trans_id);
                $stmt_add_log->bindParam(':request_data', $request_data);
                $stmt_add_log->execute();
            } catch (PDOException $e) {
                $this->warning .= "<p style='color: red;font-size: 15px;'>An error occured</p>";
                $this->field_error = 'An error occured';
                $this->error_code = $this->error_general;
                exit();
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
            curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if ($httpCode != 404) {
                if($migs){
                   header("location:https://netpluspay.com/payment/select");
                   exit();
               }else{
                header("location:https://netpluspay.com/payment/select_bank");
                exit();
            }
            } else {
                $this->warning .= "<p style='color: red;font-size: 15px;'>An error occured: Can't connect to Return URL " . $httpCode . " " . $url . " </p>";
                $this->error_code = $this->error_unknown_response_domain_field;
                $this->field_error = 'Unknown Domain';
                session_destroy();
            }
        } else {
            $this->warning .= "<p style='color: red;font-size: 15px;'>Missing parameter:" . $this->field_error . "</p>";
            session_destroy();
        }
    }

    public function logout($bank)
    {

        try {
            $trans_id = $this->decrypt_data($_SESSION['order_request']['trans_id']);
            $stmt = $this->dbh->prepare("UPDATE transaction_details SET transaction_status = 'Cancelled' WHERE trans_id = :transaction_id");
            $stmt->bindParam(':transaction_id', $trans_id);
            $stmt->execute();
        } catch (PDOException $e) {

        }

        ?>

        <body onload="document.submit2gtpay_form.submit()">
        <form method="post" name="submit2gtpay_form"
              action="<?php echo $this->decrypt_data($_SESSION['order_request']['return_url']); ?>">
            <input type="hidden" value="Transaction cancelled by user" name="description">
            <input type="hidden" value="<?php echo $this->error_transaction_cancelled; ?>" name="code">
            <input type="hidden" value="<?php echo $this->decrypt_data($_SESSION['order_request']['trans_id']) ?>"
                   name="transaction_id">
            <input type="hidden" value="<?php echo $this->decrypt_data($_SESSION['order_request']['order_id']) ?>"
                   name="order_id">
            <input type="hidden" value="0"
                   name="amount_paid">
            <input type="hidden" value="<?php echo $bank ?>" name="bank">
        </form>
        </body>
    <?php
    }

    public function generate_auth_payload($data){
        $variables = $data['transction_id'] . '|' . $data['amount'] . '|' . $data['timestamp'];
        $hash_to_be_shaed = $data['transction_id'] . '|' . $data['amount'] . '|' . $data['key'];
        $hashed_variables = hash('sha512', $hash_to_be_shaed);
        return base64_encode($variables.'|'.$hashed_variables);
    }

    public function decode_response_payload($data,$key){
        $variables = base64_decode($data);
        $variablesArray = explode("|",$variables);

        $dataArray = explode("|",$variablesArray[0]);

        $string = '';
        $loop = 0;
        do{
            $string .= $dataArray[$loop];
            $loop++;
        }while(count($dataArray)>$loop);

        $newHash = hash('sha512', $string.$key);
        if($newHash==$variablesArray[1]){
            return $dataArray;
        } else {
            return '0';
        }

    }
    private function logTransactionDetails($data){
        if(isset($data['trans_id']) && !empty($data['trans_id'])){
            $this->trans_id = $data['trans_id'];
        }
        if(isset($data['order_id']) &&  !empty($data['order_id'])){
            $this->order_id = $data['order_id'];
        }
        if(isset($data['full_name']) &&  !empty($data['full_name'])){
            $this->full_name = $data['full_name'];
        }
        if(isset($data['email']) &&  !empty($data['email'])){
            $this->email = $data['email'];
        }
        if(isset($data['total_amount']) &&  !empty($data['total_amount'])){
            $this->total_amount = $data['total_amount'];
        }
        if(isset($data['order_time']) &&  !empty($data['order_time'])){
            $this->order_time = $data['order_time'];
        }
        if(isset($data['merchant_id']) &&  !empty($data['merchant_id'])){
            $this->merchant_id = $data['merchant_id'];
        }
        if(isset($data['narration']) &&  !empty($data['narration'])){
            $this->narration = $data['narration'];
        }
        $created_at = date('Y-m-d h:i:s');
        $stmt_add_log = $this->dbh->prepare("INSERT INTO transaction_details (trans_id, order_id, full_name, email, amount, order_time, merchant_id, bank_id, narration, created_at) VALUES (:trans_id, :order_id, :full_name, :email, :amount, :order_time, :merchant_id, :bank_id, :narration, :created_at)");
        $stmt_add_log->bindParam(':trans_id', $this->trans_id);
        $stmt_add_log->bindParam(':order_id', $this->order_id);
        $stmt_add_log->bindParam(':full_name', $this->full_name);
        $stmt_add_log->bindParam(':email', $this->email);
        $stmt_add_log->bindParam(':amount', $this->total_amount);
        $stmt_add_log->bindParam(':order_time', $this->order_time);
        $stmt_add_log->bindParam(':merchant_id', $this->merchant_id);
        $stmt_add_log->bindParam(':bank_id', $data['bank']);
        $stmt_add_log->bindParam(':narration', $this->narration);
        $stmt_add_log->bindParam(':created_at', $created_at);
        $stmt_add_log->execute();
    }



    protected function decryptData($data){
        $decryptedData = array();
        if (isset($_SESSION['order_request']['merchant_id']) && !empty($_SESSION['order_request']['merchant_id'])) {
            $decryptedData['merchant_id'] = $this->decrypt_data($_SESSION['order_request']['merchant_id']);
        }
        if (isset($_SESSION['order_request']['full_name']) && !empty($_SESSION['order_request']['full_name'])) {
            $decryptedData['full_name'] = $this->decrypt_data($_SESSION['order_request']['full_name']);
        }
        if (isset($_SESSION['order_request']['email']) && !empty($_SESSION['order_request']['email'])) {
            $decryptedData['email'] = $this->decrypt_data($_SESSION['order_request']['email']);
        }
        if (isset($_SESSION['order_request']['total_amount']) && !empty($_SESSION['order_request']['total_amount'])) {
            $decryptedData['total_amount'] = $this->decrypt_data($_SESSION['order_request']['total_amount']);
        }
        if (isset($_SESSION['order_request']['currency_code']) && !empty($_SESSION['order_request']['currency_code'])) {
            $decryptedData['currency_code'] = $this->decrypt_data($_SESSION['order_request']['currency_code']);
        }
        if (isset($_SESSION['order_request']['narration']) && !empty($_SESSION['order_request']['narration'])) {
            $decryptedData['narration'] = $this->decrypt_data($_SESSION['order_request']['narration']);
        }
        if (isset($_SESSION['order_request']['order_id']) && !empty($_SESSION['order_request']['order_id'])) {
            $decryptedData['order_id'] = $this->decrypt_data($_SESSION['order_request']['order_id']);
        }
        if (isset($_SESSION['order_request']['return_url']) && !empty($_SESSION['order_request']['return_url'])) {
            $decryptedData['return_url'] = $this->decrypt_data($_SESSION['order_request']['return_url']);
        }
        if (isset($_SESSION['order_request']['trans_id']) && !empty($_SESSION['order_request']['trans_id'])) {
            $decryptedData['trans_id'] = $this->decrypt_data($_SESSION['order_request']['trans_id']);
        }
        if (isset($_SESSION['order_request']['bank']) && !empty($_SESSION['order_request']['bank'])) {
            $decryptedData['bank'] = $this->decrypt_data($_SESSION['order_request']['bank']);
        }

        return $decryptedData;
    }

}

?>
